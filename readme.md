# Camada de Interface com a Unimed

instruções

composer install<br />

copiar o .env.example para um novo arquivo .env <br />

php artisan vendor:publish --provider="JeroenNoten\LaravelAdminLte\ServiceProvider" --tag=assets

Criar procedure para gerar relatorios de visitas unicas: 

```
CREATE PROCEDURE `getStatsUnicos`(IN dateI datetime, IN dateF datetime,IN tabela varchar(40))
BEGIN
	set @sqlStr =  CONCAT('select lp_id,lp_date from ',tabela,' where lp_date between "',dateI,'" and "',dateF,'" group by lp_ip');
	PREPARE stmt FROM @sqlStr;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END
```
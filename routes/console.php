<?php

use Illuminate\Foundation\Inspiring;
use App\Console\Commands\AtualizaBaseCommand;
//use App\Console\Commands\CorrigeCardId;
//use App\Console\Commands\CronMetricasClientes;
use App\Console\Commands\CronInconsistencias;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('atualiza-base',function(){
    AtualizaBaseCommand::atualizaBase();
});

//Artisan::command('atualiza-card-id',function(){
//    CorrigeCardId::corrigeCardId();
//});
//
//Artisan::command('atualiza-cliente-metricas',function(){
//    CronMetricasClientes::atualizaBaseClienteMetricas();
//});

Artisan::command('checkIncosistencias',function(){
    (new CronInconsistencias())->process();
});
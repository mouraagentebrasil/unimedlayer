<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/','AuthController@login');

Route::group(['middleware' => ['web','isLogged']], function () {
    Route::post('/searchLead', 'LeadController@searchLead');

    Route::get('/newsearch', function () {
        return view('spa.newsearch');
    });

    Route::get('/criaCarteiras', function () {
        return view('spa.criacarteirinhas');
    });

    Route::get('/auditoriaCarteirasPorUsuario', function () {
        return view('spa.auditoriacarteirasporusuario');
    });

    Route::get('/exclusaoCarteiras', function () {
        return view('spa.exclusaocarteirinhas');
    });

    Route::post('/excluiCarteira','CarteiraController@exclusaoCarteira');

    Route::get('/findAuditoriasPorUsuario/{data_inicial?}/{data_final?}','AuditoriaUnimedController@getAuditoriasByUser');

    Route::post('/geraCarteiras','CarteiraController@geraCarteiras');

    Route::get('/operacoesUsuario', function () {
        return view('spa.operacoesusuarios');
    });

    Route::get('/getInfoLeadOrCpf/{value}/{opcao}','LeadController@getInfoLeadOrCpf');

    Route::post('/geraCarteiraUnicaUnimed','CarteiraIndividualController@geraCarteiraUnicaUnimed');

    Route::get('/logout','AuthController@logout');

    Route::get('/auditoriaAutomatica', function () {
        return view('spa.listajobauditoriadiaria');
    });

    Route::get('/getAuditoriasAutomaticas/{date?}','AuditoriaIncosistencia@getAuditoriaDaily');

    Route::post('/generateRel','AuditoriaIncosistencia@generateRel');

    Route::get('/downloadRel/{fileName}','AuditoriaIncosistencia@downloadRel');

});
Route::get('/getGraphsLeadsRange/{product}/{dateI?}/{dateF?}/{campaing?}','GraphsLeadController@getObjectGraphLeadQuantityRange');

Route::get('/indicadores', function () {
        return view('spa.indicadores');
});


Route::get('/getStatsCampaing/{product}','GraphsDailyController@getStatsCampaing');

Route::get('/getGraphsStatsDay/{product}/{date}','GraphsDailyController@getObjectGraphStatsDate');

Route::get('/getStatsPerMonths/{product}/{monthYear}','GraphsDailyController@getStatsPerMonths');

Route::get('/getStatsPerYear/{product}/{year}','GraphsDailyController@getStatsPerYear');

Route::get('/getStatsDaysRange/{product}/{dateI}/{dateF?}','GraphsDailyController@getStatsDayInRange');

Route::get('/getStatsDaysRangeByCampaing/{product}/{campaing}/{dateI}/{dateF?}','GraphsDailyController@getStatsDayInRangeByCampaing');

Route::get('/getLeadsDailyRange/{product}/{campaing}/{dataI}/{dataF?}','GraphsLeadController@getLeadsDailyRange');

Route::get('/getDailyLeadsPerDate/{product}/{dataI}','GraphsLeadController@getDailyLeadsPerDate');

Route::get('/getGraphsCampaing/{product}','GraphsDailyController@getCampaing');

Route::get('/getLeadsConvertions/{product}/{campaing}/{dateI}/{dateF?}','GraphsLeadController@getLeadsConvertions');

Route::get('/login', 'AuthController@login');

Route::post('/auth','AuthController@auth');

Route::get('/gatewayAb/getInfoLeadPlan/{lead_id}','GatewayBoletos@getInfoLeadPlan');

Route::get('/gatewayAb/getLinhaDigitavel/{cobranca}','GatewayBoletos@getLinhaDigitavel');


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comercial extends Model
{
    //
    protected $table = 'comercial';
    protected $guarded = ['come_id'];
    public $timestamps = false;

    public static function findLoginValid($login,$pass){
        $query = self::where('come_email',$login)
            ->where('come_pass',$pass)->get();
        return $query;
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Carteira;

class AtualizaBaseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autualiza-base';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza a base';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public static function atualizaBase()
    {

        $file_name_log = "atualizacao_".date('Y_m_d')."_".".txt";
        $updateCard = function($objCarteira) use ($file_name_log) {
            $str_query = "";
            $str = "nao_importado";
            if(strrpos($objCarteira->card_linha_txt,"excluído com sucesso.")==true){
                $str = "cancelado";
            }
            elseif(strrpos($objCarteira->card_linha_txt,"importada com sucesso.")==true){
                $str = "ativo";
            }
            $update = Carteira::where('card_id',$objCarteira->card_id)->update(['card_status'=>$str]);
            if($update){
                $str_query = "update carteirinha SET card_status='{$str}' WHERE card_id={$objCarteira->card_id} (OK)";
                file_put_contents(storage_path("logs/atualiza_base/".$file_name_log),$str_query.PHP_EOL,FILE_APPEND);
            } else {
                $str_query = "update carteirinha SET card_status='{$str}' WHERE card_id={$objCarteira->card_id} (ERROR)";
                file_put_contents(storage_path("logs/atualiza_base/".$file_name_log),$str_query.PHP_EOL,FILE_APPEND);
            }
            return $str_query;
        };

        $allCarteiras = Carteira::all();
        for($i=0;$i<count($allCarteiras);$i++){
            echo $updateCard($allCarteiras[$i])."\n";
        }
        echo "\nFinalizado";
        exit;
    }
}

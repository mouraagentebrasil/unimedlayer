<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use App\Http\Controllers\LeadController;
use Swift_SmtpTransport;
use Swift_Mailer;

class CronInconsistencias extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'checkIntegrity';
    protected $client = null;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para checar a integridade da base de dados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new Client();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
    }

    public function sendMail($objQueue)
    {
        if($objQueue->fl_finalizado==1) return false;
        $transport = \Swift_SmtpTransport::newInstance('smtp.agentebrasil.com.br','587','')
            ->setUsername(getenv('MAIL_USERNAME'))
            ->setPassword(getenv('MAIL_PASSWORD'));
        // Create the Mailer using your created Transport
        $mailer = \Swift_Mailer::newInstance($transport);

        // Create a message
        $message = \Swift_Message::newInstance('CRON - CheckInconsistencias Terminou')
            ->setFrom(array('noreply@agentebrasil.com.br' => 'SysV2'))
            ->setTo(array(
                'felipemouracv@gmail.com' => 'Felipe',
                'lucassbranco@gmail.com'=>'Lucas'
            ))
            ->setBody('Finalizado<br /> procecesso durou: '.
                ($objQueue->timestamp_finally - $objQueue->timestamp_init).
                ' segundos em '.$objQueue->number_process.' vezes','text/html');
        $numSent = $mailer->send($message);
        if($numSent){
            DB::table('queue_inconsistencias')->where('queue_inc_id',$objQueue->queue_inc_id)->update(['fl_finalizado'=>1]);
        }
    }

    public function setProcess()
    {
        $countLeads = DB::table('leads')->where('lead_status','Cliente')->count();
        $number_process = ceil($countLeads / 500);
        $queue = DB::table('queue_inconsistencias')->where('date_process',date('Y-m-d'))->get();
        //var_dump(count($queue)); exit;

        $objQueue = new \stdClass();
        if(count($queue) >= 1){
            if($queue[0]->status_number_process <= $queue[0]->number_process){
                $queueDB = DB::table('queue_inconsistencias')->where('date_process',date('Y-m-d'));
                $inc = $queueDB->increment('status_number_process');
                if($queue[0]->status_number_process+1 == $queue[0]->number_process) {
                    $finallyDB = DB::table('queue_inconsistencias')
                        ->where('date_process',date('Y-m-d'))
                        ->update(['timestamp_finally'=>time()]);
                }
            }
        } else {
            $queueDB = DB::table('queue_inconsistencias')->insert([
                'offset'=>500,'limit'=>500,'date_process'=>date('Y-m-d'),
                'number_process'=>$number_process,'status_number_process'=>1,
                'timestamp_init'=>time()
            ]);
            $queueDB = DB::table('queue_inconsistencias')->where('date_process',date('Y-m-d'));
        }
        $objQueue = DB::table('queue_inconsistencias')->where('date_process',date('Y-m-d'))->get();
        //var_export($objQueue); exit;
        return $objQueue[0];
    }

    public function processChecks($objQueue)
    {
        //$processID = $this->getProcessID();
        $file_name = "queue_".$objQueue->queue_inc_id."-".$objQueue->date_process."-".time().".txt";
        $leads = DB::table('leads')
            ->where('lead_status','Cliente')
            ->offset($objQueue->offset*$objQueue->status_number_process)
            ->limit($objQueue->limit)
            ->get();
        //verificação apenas para lead_status = Cliente
        $verificaTitularDuplicadoEnumBeneficiariosPorLeadQtd = function($lead) {
            $num_plan = $lead->lead_quantidade;
            $bene = DB::table('beneficiarios')->where('lead_id',$lead->lead_id)->count();
            $titular = DB::table('titulares')->where('lead_id',$lead->lead_id)->get();
            $total_benefiaciarios = $bene + count($titular);
            //primeiro erro
            if($total_benefiaciarios > $num_plan){
                $msg = "Numero de beneficiarios(titular + beneficiarios) execede o determinado em lead_quantidade";
                return DB::table('auditoria_inconsistencias')->insert([
                    'lead_id'=>$lead->lead_id,
                    'data_processamento'=>date('Y-m-d H:i:s'),
                    'status_valor'=>'mais',
                    'desc_erro'=>$msg,
                ]);
            } elseif($total_benefiaciarios < $num_plan) {
                $msg = "Numero de beneficiarios é inferior ao determindao em lead_quantidade";
                return DB::table('auditoria_inconsistencias')->insert([
                    'lead_id'=>$lead->lead_id,
                    //'id_tipo'=>$titular[0]->titu_id,
                    //'tipo'=>'titular',
                    'data_processamento'=>date('Y-m-d H:i:s'),
                    'status_valor'=>'menos',
                    'desc_erro'=>$msg
                ]);
            }
            return false;
        };
        //verificação apenas para lead_status = Cliente
        $vericaTitularClienteSemCardUnimedID = function($lead) {
            $titular = DB::table('titulares')->where('lead_id',$lead->lead_id)->get();
            //titular duplicado é trato em outro momento
            if(count($titular) > 1) return null;
            if(count($titular)==0) {
                //o lead não possui nenhum titular
                return DB::table('auditoria_inconsistencias')->insert([
                    'lead_id'=>$lead->lead_id,
                    'data_processamento'=>date('Y-m-d H:i:s'),
                    'desc_erro'=>'Usuario sem titular cadastrado'
                ]);
            }
            if($titular[0]->card_id == "" && $lead->lead_status =="Cliente"){
                return DB::table('auditoria_inconsistencias')->insert([
                    'lead_id'=>$lead->lead_id,
                    'id_tipo'=>$titular[0]->titu_id,
                    'tipo'=>'titular',
                    'data_processamento'=>date('Y-m-d H:i:s'),
                    'desc_erro'=>'Titular sem card_id (CARD_UNIMED)'
                ]);
            }
            return false;
        };
        //verificação apenas para lead_status = Cliente
        $verificaBeneficiariosClienteSemCardUnimedID = function ($lead){
            $beneficiarios = DB::table('beneficiarios')->where('lead_id',$lead->lead_id)->get();
            if(count($beneficiarios)==0) return false;
            if(count($beneficiarios)>0){
                for($i=0;$i<count($beneficiarios);$i++){
                    //var_dump($beneficiarios[$i]); exit;
                    if($beneficiarios[$i]->card_id == ""){
                        DB::table('auditoria_inconsistencias')->insert([
                            'lead_id'=>$lead->lead_id,
                            'id_tipo'=>$beneficiarios[$i]->bene_id,
                            'tipo'=>'beneficiario',
                            'data_processamento'=>date('Y-m-d H:i:s'),
                            'desc_erro'=>'Beneficiario sem card_id (CARD_UNIMED)'
                        ]);
                    }
                }
                return true;
            }
            return false;
        };
        //vericações ab->unimed
        $getTitularByLead = function($lead){
              $titular = DB::table('titulares')->where('lead_id')->get();
              return count($titular) > 1 || count($titular) == 0 ? null : $titular[0];
        };

        $getBeneficiariosByLead = function($lead){
            $beneficiarios = DB::table('beneficiarios')->where('lead_id',$lead->lead_id)->get();
            return count($beneficiarios)==0 ? null : $beneficiarios;
        };

        $processBeneficiarios = function($beneficiarios){
            for($i=0;$i<count($beneficiarios);$i++){
                $status = $this->processUnimedStatus($beneficiarios[$i]->card_id);
                if($status=="Excluído"){
                    DB::table('auditoria_inconsistencias')->insert([
                        'lead_id'=>$beneficiarios[$i]->lead_id,
                        'id_tipo'=>$beneficiarios[$i]->bene_id,
                        'tipo'=>'beneficiario',
                        'data_processamento'=>date('Y-m-d H:i:s'),
                        'status_valor'=>'menos',
                        'desc_erro'=>"Beneficiario de cliente ativo com carteira Excluída na Unimed",
                        'card_unimed'=>$beneficiarios[$i]->card_id
                    ]);
                }
            }
        };

        $processTitular = function($titular){
            $status = $this->processUnimedStatus($titular->card_id);
            if($status=="Excluído"){
                DB::table('auditoria_inconsistencias')->insert([
                    'lead_id'=>$titular->lead_id,
                    'id_tipo'=>$titular->titu_id,
                    'tipo'=>'titular',
                    'data_processamento'=>date('Y-m-d H:i:s'),
                    'status_valor'=>'menos',
                    'desc_erro'=>"Titular de cliente ativo com carteira Excluída na Unimed",
                    'card_unimed'=>$titular->card_id
                ]);
            }
        };

        $checkStatusUnimed = function($lead) use ($getBeneficiariosByLead,$getTitularByLead,$processTitular,$processBeneficiarios){
            //falta processar os dados
            if($lead->card_id != "" && $lead->lead_cobrado=="titular_pagador"){
//                $status = $this->processUnimedStatus($lead->card_id);
//                if($status=="Excluído"){
//                    DB::table('auditoria_inconsistencias')->insert([
//                        'lead_id'=>$lead->lead_id,
//                        'data_processamento'=>date('Y-m-d H:i:s'),
//                        'status_valor'=>'menos',
//                        'desc_erro'=>"Titular Pagador de cliente ativo com carteira Excluída na Unimed",
//                        'card_unimed'=>$lead->card_id
//                    ]);
//                }
            } else {
                $titular = $getTitularByLead($lead);
                if(!is_null($titular)){
                    $processTitular($getTitularByLead($lead));
                }
            }
            $beneficiarios = $getBeneficiariosByLead($lead);
            if(!is_null($beneficiarios)){
                $processBeneficiarios($beneficiarios);
            }
        };

        for($i=0;$i<count($leads);$i++):
            if($leads[$i]->lead_status = "Cliente") {
                $verificaTitularDuplicadoEnumBeneficiariosPorLeadQtd($leads[$i]);
                $vericaTitularClienteSemCardUnimedID($leads[$i]);
                $verificaBeneficiariosClienteSemCardUnimedID($leads[$i]);
                $checkStatusUnimed($leads[$i]);
            }
        endfor;
    }

    public function process()
    {
        $process = $this->setProcess();

        if($process->status_number_process <= $process->number_process){
            try {
                $this->processChecks($process);
            } catch (\Exception $e){
                var_dump($e->getLine());
                var_dump($e->getFile());
                var_dump($e->getMessage());
            }
        } else {
            $this->sendMail($process);
            echo "\nfinalizado\n";
        }
    }

    public function getStatusUnimed($card_id)
    {
        return (new LeadController())->getXmlConsultarPropostaPf(['type'=>'card','value'=>$card_id]);
    }

    public function processUnimedStatus($card_id)
    {
        $url = getenv("WEB_SERVER_UNIMED");
        try {
            $r = $this->client->request('POST', $url, [
                'headers'=> [
                    'Content-Type' => 'text/xml; charset=UTF8'
                ],
                'body' => $this->getStatusUnimed($card_id)
            ]);

            $xml = new \GreenCape\Xml\Converter($r->getBody()->getContents());
            $data = $xml->data;
            return $this->formataResposta($data);
        } catch (\Exception $e){
            return "Erro";
        }
    }

    public function formataResposta($data)
    {
        return isset($data["soap:Envelope"]["soap:Body"]["ConsultarPropostaPfResponse"]["ConsultarPropostaPfResult"][1]["Contrato"][2]['Beneficiarios'][3]['Situacao']) ?
            $data["soap:Envelope"]["soap:Body"]["ConsultarPropostaPfResponse"]["ConsultarPropostaPfResult"][1]["Contrato"][2]['Beneficiarios'][3]['Situacao'] : "Erro";
    }
}

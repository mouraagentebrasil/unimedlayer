<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plano extends Model
{
    //
    protected $guarded = ['plan_id'];
    protected $table = 'planos';
    public $timestamps = false;

    public static function getPlanoById($plan_logica)
    {
        $plan_id = self::where('plan_logica',$plan_logica)->get()->toArray()[0];
        if(empty($plan_id)) return [];
        return $plan_id;
    }
}

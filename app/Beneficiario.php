<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beneficiario extends Model
{
    //
    protected $guarded = ['bene_id'];
    protected $table = 'beneficiarios';
    public $timestamps = false;
}

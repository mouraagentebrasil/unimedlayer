<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Titular extends Model
{
    //
    protected $guarded = ['titu_id'];
    protected $table = 'titulares';
    public $timestamps = false;
}

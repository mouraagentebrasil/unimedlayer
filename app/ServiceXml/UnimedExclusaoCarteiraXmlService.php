<?php
/**
 * Created by PhpStorm.
 * User: felipemoura
 * Date: 18/10/2016
 * Time: 17:13
 */

namespace App\ServiceXml;


class UnimedExclusaoCarteiraXmlService
{
    protected $data_exclusao;
    protected $carteira;
    protected $observacao;
    protected $motivo;
    protected $origem;

    public function __construct($data_exclusao,$carteira,$observacao,$motivo)
    {
        $this->data_exclusao = $data_exclusao;
        $this->carteira = $carteira;
        $this->observacao = $observacao;
        $this->motivo = $motivo;
    }

    public function getXmlTemplateExclusaoBeneficiario()
    {
        $input_xml ='<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:top="http://www.topdown.com.br">
                      <soap:Header/>
                      <soap:Body>
                        <top:ExclusaoBeneficiario>
                          <top:exclusaoBeneficiario>
                            <top:NumBeneficiario>'.$this->carteira.'</top:NumBeneficiario>
                            <top:DataExclusao>'.(string)$this->data_exclusao.'</top:DataExclusao>
                            <top:CodMotivoExclusao>'.$this->motivo.'</top:CodMotivoExclusao>
                            <top:ObservacaoExclusao>'.$this->observacao.'</top:ObservacaoExclusao>
                            <top:IndContributario>N</top:IndContributario>
                          </top:exclusaoBeneficiario>
                        </top:ExclusaoBeneficiario>
                      </soap:Body>
                    </soap:Envelope>';
        //var_dump($input_xml); exit;
        return $input_xml;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: felipemoura
 * Date: 20/10/2016
 * Time: 11:56
 */

namespace App\ServiceXml;


class UnimedGeraPropostaIndividualXmlService
{

    public function processXmlResponsavel($data = null)
    {
        $responsavel_financeiro='
            <top:Responsavel>'.
            '<top:Nome></top:Nome>'.
            '<top:Cpf></top:Cpf>'.
            '<top:DataNascimento></top:DataNascimento>'.
            '<top:EstadoCivil>0</top:EstadoCivil>'.
            '<top:GrauParentesco>0</top:GrauParentesco>'.
            '<top:Sexo></top:Sexo>'.
            '<top:Email></top:Email>'.
            '</top:Responsavel>';
        if(!is_null($data)) {
            $responsavel_financeiro='<top:Responsavel>'.
                '<top:Nome>'.$data['nome'].'</top:Nome>'.
                '<top:Cpf>'.$data['cpf'].'</top:Cpf>'.
                '<top:DataNascimento>'.$data['nascimento'].'</top:DataNascimento>'.
                '<top:EstadoCivil>'.($data['civil'] == "solteiro" ? 1: 2).'</top:EstadoCivil>'.
                '<top:GrauParentesco>0</top:GrauParentesco>'.
                '<top:Sexo>'.($data['sexo'] == "femenino" ? "F" : "M").'</top:Sexo>'.
                '<top:Email>'.$data['email'].'</top:Email>'.
                '</top:Responsavel>';
        }
        return $responsavel_financeiro;
    }

    public function processXmlTitulares($titular)
    {
        $titulares = "";
        $titulares = '<top:Titular>'.
            '<top:Nome>'.$titular['nome'].'</top:Nome>'.
            '<top:Cpf>'.$titular['cpf'].'</top:Cpf>'.
            ' <top:EstadoCivil>'.$titular['civil'].'</top:EstadoCivil>'.
            '<top:Sexo>'.$titular['sexo'].'</top:Sexo>'.
            '<top:NomeMae>'.$titular['mae'].'</top:NomeMae>'.
            '<top:DataNascimento>'.$titular['nascimento'].'</top:DataNascimento>'.
            '<top:Rg>'.
            '<top:Registro>'.null.'</top:Registro>'.
            '<top:OrgaoEmissor>'.null.'</top:OrgaoEmissor>'.
            '</top:Rg>'.
            '<top:NumAssociadoOperadora>'.$titular['card_id'].'</top:NumAssociadoOperadora>'.
            '<top:Endereco>'.
            '<top:Logradouro>'.$titular['logradouro'].'</top:Logradouro>'.
            '<top:Numero>'.$titular['numero'].'</top:Numero>'.
            '<top:Complemento>'.$titular['complemento'].'</top:Complemento>'.
            '<top:Cep>'.$titular['cep'].'</top:Cep>'.
            '<top:Bairro>'.$titular['bairro'].'</top:Bairro>'.
            '<top:Cidade>'.$titular['cidade'].'</top:Cidade>'.
            '<top:Uf>'.$titular['uf'].'</top:Uf>'.
            '</top:Endereco>'.
            '<top:Contato>'.
            '<top:TelefoneResidencial>'.$titular['fone_full'].'</top:TelefoneResidencial>'.
            '<top:TelefoneComercial>'.null.'</top:TelefoneComercial>'.
            '<top:RamalComercial>'.null.'</top:RamalComercial>'.
            '<top:Fax>'.null.'</top:Fax>'.
            '<top:TelefoneCelular>'.$titular['fone_full'].'</top:TelefoneCelular>'.
            '<top:Email>'.$titular['email'].'</top:Email>'.
            '</top:Contato>'.
            '</top:Titular>';
        return $titulares;
    }

    //Todo Unificar essa método em UnimedGeraPropostaXmlService
    public function calculaIdade($dataNascimento)
    {
        $geraTimestamp = function($data) {
            $partes = explode('-', $data);
            return mktime(0, 0, 0, $partes[1], $partes[2], $partes[0]);
        };

        $time_inicial = $geraTimestamp($dataNascimento);
        $time_final = $geraTimestamp(date('Y-m-d'));
        $diferenca = $time_final - $time_inicial; // 19522800 segundos
        $dias = (int)floor( $diferenca / (60 * 60 * 24)); // 225 dias
        return $dias/365;
    }

    public function processaXmlSoapUnimedGeraPropostaIndividual($titularXml,$responsavelXml,$dataOperacao, $planID)
    {
        $titularXml = $this->processXmlTitulares($titularXml);

        $responsavelXml = $this->processXmlResponsavel($responsavelXml);

        return $this->totalXmlGeraUnimedPropostaIndividual($titularXml,$responsavelXml,$dataOperacao,$planID);
    }

    public function totalXmlGeraUnimedPropostaIndividual($titularXml,$responsavelXml,$dataOperacao, $planID)
    {
//        $getCampanha = function($data) {
//            if($data >= '2016-08-01'){
//                return 162;
//            } elseif($data <= '2016-07-31' ){
//                return 15;
//            }
//        };

        $getCampanhaValor = function($data,$planID) {
            if($data >= '2016-11-01' && $planID == 459 ){
                return ['campID'=>376,'valor'=>"42,90"];
            }elseif($data >= '2016-11-01' && $planID == 460 ){
                return ['campID'=>402,'valor'=>"59,90"];
            }elseif($data >= '2016-08-01'){
                return ['campID'=>162,'valor'=>0];
            } elseif($data <= '2016-07-31' ){
                return ['campID'=>15,'valor'=>0];
            }
        };

        $input_xml =
            '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:top="http://www.topdown.com.br">
                  <soapenv:Header/>
                  <soapenv:Body>
                    <top:ImportarPropostaPf>
                      <top:propostaPf>
                        <top:IndOrigem>C</top:IndOrigem>
                        <top:NumeroProposta></top:NumeroProposta>
                        <top:CodOperadora>1</top:CodOperadora>
                        <top:CodFilial>1</top:CodFilial>
                        <top:CodCampanha>'.$getCampanhaValor($dataOperacao,$planID)['campID'].'</top:CodCampanha>
                        <top:CodUnidade>1</top:CodUnidade>
                        <top:CodCobertura>4</top:CodCobertura>
                        <top:TipoVenda>1</top:TipoVenda>
                        <top:DataVenda>'.$dataOperacao.'</top:DataVenda>
                        <top:DadosComissionado>
                          <top:CodCorretora>16763</top:CodCorretora>
                          <top:CpfCorretor></top:CpfCorretor>
                          <top:CpfGerente></top:CpfGerente>
                        </top:DadosComissionado>
                        <top:TotalizadorCobranca>
                          <top:ValorTotalMensalidadeSaude>0</top:ValorTotalMensalidadeSaude>
                          <top:ValorTotalMensalidadeDental>0</top:ValorTotalMensalidadeDental>
                          <top:ValorTotalAditivos>0</top:ValorTotalAditivos>
                          <top:ValorTotalProposta>0</top:ValorTotalProposta>
                          <top:ValorTotalDescontos>0</top:ValorTotalDescontos>
                        </top:TotalizadorCobranca>
                        <top:DadosPagamento>
                          <top:CodTipoCobranca>2</top:CodTipoCobranca>
                          <top:QtdParcelas>1</top:QtdParcelas>
                          <top:QtdMensalidades>1</top:QtdMensalidades>
                          <top:CobrancaCartaoCredito>
                            <top:IdTransacao></top:IdTransacao>
                            <top:ValorPagamento>0</top:ValorPagamento>
                            <top:QuantidadeParcelas>0</top:QuantidadeParcelas>
                          </top:CobrancaCartaoCredito>
                          <top:CobrancaBoleto>
                            <top:ValorPagamento>0</top:ValorPagamento>
                            <top:Efaturamento>0</top:Efaturamento>
                            <top:QuantidadeParcelas>0</top:QuantidadeParcelas>
                          </top:CobrancaBoleto>
                          <top:DebitoAutomatico>
                            <top:CodBanco></top:CodBanco>
                            <top:CodAgencia></top:CodAgencia>
                            <top:NumContaCorrente></top:NumContaCorrente>
                            <top:NumDvAgencia></top:NumDvAgencia>
                            <top:NumDvCC></top:NumDvCC>
                          </top:DebitoAutomatico>
                        </top:DadosPagamento>
                        '.$responsavelXml.'
                        <top:Contrato>
                          <top:PlanoContratadoSaude>
                            <top:CodPlano></top:CodPlano>
                            <top:CodGrupoCarencia></top:CodGrupoCarencia>
                          </top:PlanoContratadoSaude>
                          <top:PlanoContratadoDental>
                            <top:CodPlano>'
            .$planID.
            '</top:CodPlano>
                            <top:CodGrupoCarencia></top:CodGrupoCarencia>
                          </top:PlanoContratadoDental>
                          <top:ValorCobradoTitular>'
            .$titularXml.
            '<top:ValorCobrado>
                              <top:ValorDental>
                                <top:Valor>'.$getCampanhaValor($dataOperacao,$planID)['valor'].'</top:Valor>
                              </top:ValorDental>
                            </top:ValorCobrado>
                          </top:ValorCobradoTitular>
                        </top:Contrato>
                      </top:propostaPf>
                    </top:ImportarPropostaPf>
                  </soapenv:Body>
                </soapenv:Envelope>';

        return $input_xml;
    }


}
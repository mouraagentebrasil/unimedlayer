<?php

namespace App\ServiceXml;

class UnimedGeraPropostaXmlService {

    protected $lead;
    protected $titular;
    protected $beneficiario;

    public function __construct($lead,$titular,$beneficiario,$data_inclusao)
    {
        $this->lead = $lead;
        $this->titular = $titular;
        $this->beneficiario = $beneficiario;
        $this->data_inclusao = $data_inclusao;
    }

    public function getXmlResponsavel()
    {
        $titulares = $this->getTitularesMerged();
        $responsavel_financeiro = [];
        for($i=0;$i<count($titulares);$i++){
            $responsavel_financeiro[$i] = $this->getXmlTemplateResponsavel(false);
            if($this->calculaIdade($titulares[$i]['nascimento']) < 18) {
                $responsavel_financeiro[$i] = $this->getXmlTemplateResponsavel(true);
            }
        }
        return $responsavel_financeiro;
    }
    
    public function calculaIdade($dataNascimento)
    {
         $geraTimestamp = function($data) {
            $partes = explode('-', $data);
            return mktime(0, 0, 0, $partes[1], $partes[2], $partes[0]);
         };

        $time_inicial = $geraTimestamp($dataNascimento);
        $time_final = $geraTimestamp(date('Y-m-d'));
        $diferenca = $time_final - $time_inicial; // 19522800 segundos
        $dias = (int)floor( $diferenca / (60 * 60 * 24)); // 225 dias
        return $dias/365;
    }

    public function getXmlTemplateResponsavel($menorIdade = false){
        $responsavel_financeiro='
            <top:Responsavel>'.
                '<top:Nome></top:Nome>'.
                '<top:Cpf></top:Cpf>'.
                '<top:DataNascimento></top:DataNascimento>'.
                '<top:EstadoCivil>0</top:EstadoCivil>'.
                '<top:GrauParentesco>0</top:GrauParentesco>'.
                '<top:Sexo></top:Sexo>'.
                '<top:Email></top:Email>'.
            '</top:Responsavel>';
        if($menorIdade == true) {
            $responsavel_financeiro='<top:Responsavel>'.
                '<top:Nome>'.$this->lead['lead_nome'].'</top:Nome>'.
                '<top:Cpf>'.$this->lead['lead_cpf'].'</top:Cpf>'.
                '<top:DataNascimento>'.$this->lead['lead_nascimento'].'</top:DataNascimento>'.
                '<top:EstadoCivil>'.($this->lead['lead_civil'] == "solteiro" ? 1: 2).'</top:EstadoCivil>'.
                '<top:GrauParentesco>0</top:GrauParentesco>'.
                '<top:Sexo>'.($this->lead['lead_sexo'] == "femenino" ? "F" : "M").'</top:Sexo>'.
                '<top:Email>'.$this->lead['lead_email'].'</top:Email>'.
                '</top:Responsavel>';
        }
        return $responsavel_financeiro;
    }
    
    public function getXmlTemplateTitular($data)
    {
        $titulares = [];
        for($i=0;$i<count($data);$i++):
            $titulares[$i]='<top:Titular>'.
                    '<top:Nome>'.$data[$i]['nome'].'</top:Nome>'.
                    '<top:Cpf>'.$data[$i]['cpf'].'</top:Cpf>'.
                    ' <top:EstadoCivil>'.$data[$i]['civil'].'</top:EstadoCivil>'.
                    '<top:Sexo>'.$data[$i]['sexo'].'</top:Sexo>'.
                    '<top:NomeMae>'.$data[$i]['mae'].'</top:NomeMae>'.
                    '<top:DataNascimento>'.$data[$i]['nascimento'].'</top:DataNascimento>'.
                    '<top:Rg>'.
                    '<top:Registro>'.null.'</top:Registro>'.
                    '<top:OrgaoEmissor>'.null.'</top:OrgaoEmissor>'.
                    '</top:Rg>'.
                    '<top:NumAssociadoOperadora>'.$data[$i]['card_id'].'</top:NumAssociadoOperadora>'.
                    '<top:Endereco>'.
                    '<top:Logradouro>'.$data[$i]['logradouro'].'</top:Logradouro>'.
                    '<top:Numero>'.$data[$i]['numero'].'</top:Numero>'.
                    '<top:Complemento>'.$data[$i]['complemento'].'</top:Complemento>'.
                    '<top:Cep>'.$data[$i]['cep'].'</top:Cep>'.
                    '<top:Bairro>'.$data[$i]['bairro'].'</top:Bairro>'.
                    '<top:Cidade>'.$data[$i]['cidade'].'</top:Cidade>'.
                    '<top:Uf>'.$data[$i]['uf'].'</top:Uf>'.
                    '</top:Endereco>'.
                    '<top:Contato>'.
                    '<top:TelefoneResidencial>'.$data[$i]['fone_full'].'</top:TelefoneResidencial>'.
                    '<top:TelefoneComercial>'.null.'</top:TelefoneComercial>'.
                    '<top:RamalComercial>'.null.'</top:RamalComercial>'.
                    '<top:Fax>'.null.'</top:Fax>'.
                    '<top:TelefoneCelular>'.$data[$i]['fone_full'].'</top:TelefoneCelular>'.
                    '<top:Email>'.$data[$i]['email'].'</top:Email>'.
                    '</top:Contato>'.
                  '</top:Titular>';
            endfor;
        return $titulares;
    }

    public function processaXmlTitularesLead()
    {
        if($this->titular == null) return $data[0] = [];
        $data[0] = [
            'nome' => $this->titular['titu_nome'],
            'cpf' => $this->titular['titu_cpf'],
            'civil' => $this->titular['titu_civil'] == "solteiro" ? 1 : 2,
            'sexo' => $this->titular['titu_sexo'] == "feminino" ? "F": "M",
            'mae' => $this->titular['titu_mae'],
            'nascimento' => $this->titular['titu_nascimento'],
            'card_id' => $this->titular['card_id'],
            'logradouro' => $this->titular['titu_logradouro'],
            'numero' => $this->titular['titu_numero'],
            'complemento' => $this->titular['titu_complemento'],
            'cep' => $this->titular['titu_cep'],
            'bairro' => $this->titular['titu_bairro'],
            'cidade' => $this->titular['titu_cidade'],
            'uf' => $this->titular['titu_uf'],
            'fone_full' => $this->titular['titu_fone'] =="" ? $this->lead['lead_fone'] : $this->titular['titu_fone'],
            'email' => $this->titular['titu_email'],
            'tipo'=>'titular',
            'id_tipo'=>$this->titular['titu_id'],
            'lead_id'=>$this->lead['lead_id'],
            'lead_cpf'=>$this->lead['lead_cpf']
        ];

        if ($this->lead['lead_cobrado'] == "titular_pagador") {
            $data[0] = [
                'nome' => $this->titular['titu_nome'],
                'cpf' => $this->lead['lead_cpf'],
                'civil' => $this->titular['titu_civil'] == "solteiro" ? 1 : 2,
                'sexo' => $this->titular['titu_sexo'] == "feminino" ? "F": "M",
                'mae' => $this->titular['titu_mae'],
                'nascimento' => $this->titular['titu_nascimento'],
                'card_id' => $this->titular['card_id'],
                'logradouro' => $this->titular['titu_logradouro'] == "" ? $this->lead['lead_logradouro'] : $this->titular['titu_logradouro'],
                'numero' => $this->titular['titu_numero'],
                'complemento' => $this->titular['titu_complemento'],
                'cep' => $this->titular['titu_cep'],
                'bairro' => $this->titular['titu_bairro'],
                'cidade' => $this->titular['titu_cidade'],
                'uf' => $this->titular['titu_uf'],
                'fone_full' => $this->lead['lead_fone'],
                'email' => $this->lead['lead_email'],
                'tipo'=>'titular_pagador',
                'id_tipo'=>$this->titular['titu_id'],
                'lead_id'=>$this->lead['lead_id'],
                'lead_cpf'=>$this->lead['lead_cpf']
            ];
        }
        return $data;
    }

    public function processaXmlTitularesBeneficiariosLead()
    {
        if(empty($this->beneficiario)) return $data[0] = [];
        $finalXml = [];
        for($i=0;$i<count($this->beneficiario);$i++){
            $data = [
                'nome'=>$this->beneficiario[$i]['bene_nome'],
                'cpf'=>$this->beneficiario[$i]['bene_cpf'],
                'civil'=>$this->beneficiario[$i]['bene_civil'] == "solteiro" ? 1 : 2,
                'sexo'=>$this->beneficiario[$i]['bene_sexo'] == "femenino" ? "F" : "M",
                'mae'=>$this->beneficiario[$i]['bene_mae'],
                'nascimento'=>$this->beneficiario[$i]['bene_nascimento'],
                'card_id'=>$this->beneficiario[$i]['card_id'],
                'logradouro'=>$this->lead['lead_logradouro'],
                'numero'=>$this->lead['lead_numero'],
                'complemento'=>$this->lead['lead_complemento'],
                'cep'=>$this->lead['lead_cep'],
                'bairro'=>$this->lead['lead_bairro'],
                'cidade'=>$this->lead['lead_cidade'],
                'uf'=>$this->lead['lead_uf'],
                'fone_full'=>$this->lead['lead_fone'],
                'email'=>$this->lead['lead_email'],
                'tipo'=>'beneficiario',
                'id_tipo'=>$this->beneficiario[$i]['bene_id'],
                'lead_id'=>$this->lead['lead_id'],
                'lead_cpf'=>$this->lead['lead_cpf']
            ];
            $finalXml[$i] = $data;
        }
        return $finalXml;
    }
    
    public function getTitularesMerged()
    {
        return array_reverse(array_merge($this->processaXmlTitularesBeneficiariosLead(),$this->processaXmlTitularesLead()));
    }

    public function getTitularesMergedXml()
    {
        return $this->getXmlTemplateTitular($this->getTitularesMerged());
    }

    public function getXmlTemplateSoapUnimedGeraProposta()
    {
        $input_xml = [];
        for($i=0;$i<count($this->getTitularesMergedXml());$i++) :
            $getCampanhaValor = function($data,$planID) {
                if($data >= '2016-11-01' && $planID == 459 ){
                    return ['campID'=>376,'valor'=>"42,90"];
                }elseif($data >= '2016-11-01' && $planID == 460 ){
                    return ['campID'=>402,'valor'=>"59,90"];
                }elseif($data >= '2016-08-01'){
                    return ['campID'=>162,'valor'=>0];
                } elseif($data <= '2016-07-31' ){
                    return ['campID'=>15,'valor'=>0];
                }
            };
            $input_xml[$i] ='<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:top="http://www.topdown.com.br">
                  <soapenv:Header/>
                  <soapenv:Body>
                    <top:ImportarPropostaPf>
                      <top:propostaPf>
                        <top:IndOrigem>C</top:IndOrigem>
                        <top:NumeroProposta></top:NumeroProposta>
                        <top:CodOperadora>1</top:CodOperadora>
                        <top:CodFilial>1</top:CodFilial>
                        <top:CodCampanha>'.$getCampanhaValor($this->data_inclusao,$this->lead['plan_id'])['campID'].'</top:CodCampanha>
                        <top:CodUnidade>1</top:CodUnidade>
                        <top:CodCobertura>4</top:CodCobertura>
                        <top:TipoVenda>1</top:TipoVenda>
                        <top:DataVenda>'.$this->data_inclusao.'</top:DataVenda>
                        <top:DadosComissionado>
                          <top:CodCorretora>16763</top:CodCorretora>
                          <top:CpfCorretor></top:CpfCorretor>
                          <top:CpfGerente></top:CpfGerente>
                        </top:DadosComissionado>
                        <top:TotalizadorCobranca>
                          <top:ValorTotalMensalidadeSaude>0</top:ValorTotalMensalidadeSaude>
                          <top:ValorTotalMensalidadeDental>0</top:ValorTotalMensalidadeDental>
                          <top:ValorTotalAditivos>0</top:ValorTotalAditivos>
                          <top:ValorTotalProposta>0</top:ValorTotalProposta>
                          <top:ValorTotalDescontos>0</top:ValorTotalDescontos>
                        </top:TotalizadorCobranca>
                        <top:DadosPagamento>
                          <top:CodTipoCobranca>2</top:CodTipoCobranca>
                          <top:QtdParcelas>1</top:QtdParcelas>
                          <top:QtdMensalidades>1</top:QtdMensalidades>
                          <top:CobrancaCartaoCredito>
                            <top:IdTransacao></top:IdTransacao>
                            <top:ValorPagamento>0</top:ValorPagamento>
                            <top:QuantidadeParcelas>0</top:QuantidadeParcelas>
                          </top:CobrancaCartaoCredito>
                          <top:CobrancaBoleto>
                            <top:ValorPagamento>0</top:ValorPagamento>
                            <top:Efaturamento>0</top:Efaturamento>
                            <top:QuantidadeParcelas>0</top:QuantidadeParcelas>
                          </top:CobrancaBoleto>
                          <top:DebitoAutomatico>
                            <top:CodBanco></top:CodBanco>
                            <top:CodAgencia></top:CodAgencia>
                            <top:NumContaCorrente></top:NumContaCorrente>
                            <top:NumDvAgencia></top:NumDvAgencia>
                            <top:NumDvCC></top:NumDvCC>
                          </top:DebitoAutomatico>
                        </top:DadosPagamento>
                        '.$this->getXmlResponsavel()[$i].'
                        <top:Contrato>
                          <top:PlanoContratadoSaude>
                            <top:CodPlano></top:CodPlano>
                            <top:CodGrupoCarencia></top:CodGrupoCarencia>
                          </top:PlanoContratadoSaude>
                          <top:PlanoContratadoDental>
                            <top:CodPlano>'
                            .$this->lead['plan_id'].
                            '</top:CodPlano>
                            <top:CodGrupoCarencia></top:CodGrupoCarencia>
                          </top:PlanoContratadoDental>
                          <top:ValorCobradoTitular>'
                            .$this->getTitularesMergedXml()[$i].
                            '<top:ValorCobrado>
                              <top:ValorDental>
                                <top:Valor>'.$getCampanhaValor($this->data_inclusao,$this->lead['plan_id'])['valor'].'</top:Valor>
                              </top:ValorDental>
                            </top:ValorCobrado>
                          </top:ValorCobradoTitular>
                        </top:Contrato>
                      </top:propostaPf>
                    </top:ImportarPropostaPf>
                  </soapenv:Body>
                </soapenv:Envelope>';
                //grava o log do xml
                file_put_contents(
                    storage_path('logs/xmlGerado/xml_'.$this->getTitularesMerged()[$i]['lead_id'].'_ID_'.time().'.txt'),
                    $input_xml[$i]."\n\n__________________________________________________\n\n".PHP_EOL,
                    FILE_APPEND
                );
            endfor;
            //var_dump($input_xml); exit;
        return $input_xml;
    }

    public function processaXmlSoapUnimedGeraProposta()
    {
        //var_dump($this->getXmlTemplateSoapUnimedGeraProposta()); exit;
        return $this->getXmlTemplateSoapUnimedGeraProposta();
    }
}
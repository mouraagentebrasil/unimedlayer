<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Beneficiario;
use App\Titular;
use App\Lead;
use Illuminate\Support\Facades\DB;

class Carteira extends Model
{
    //
    protected $guarded = ['card_id'];
    protected $table = 'carteirinha';
    //protected $primaryKey = 'card_id';
    public $timestamps = false;


    public function insertOrUpdateCarteira($card_unimed,$message,$proposta,$dataTitular,$dataInclusao,$user_id)
    {
        //titular com card_id
        if($dataTitular['card_id']!="" && $this->checaExisteCardId($dataTitular['card_id']) == true ){
            return $this->updateCard($card_unimed,$message,$proposta,$dataTitular,$dataInclusao,$user_id);
        }
        return $this->generateNewCard($card_unimed,$message,$proposta,$dataTitular,$dataInclusao,$user_id);
    }

    public function checaExisteCardId($cardId){
        $cardId = $this->where('card_unimed',$cardId)->first();
        if(empty($cardId)) return false;
        return true;
    }
    private function saveOrUpdate($card_unimed,$tipoInsercao,$idTipo,$dataInclusao,$leadId,$message,$proposta,$user_id){

        $inc = DB::table('inclusao_carteiras')->where($tipoInsercao,$idTipo)->get();
        if(count($inc)==0){
            DB::table('inclusao_carteiras')->insert([
                'card_unimed'=>$card_unimed,
                'lead_id'=>$leadId,
                $tipoInsercao=>$idTipo,
                'retorno_unimed'=>$message,
                'proposta'=>$proposta,
                'come_id'=>$user_id,
                'data_operacao'=>date('Y-m-d H:i:s'),
                'data_inclusao_unimed'=>$dataInclusao
            ]);
        } else {
            DB::table('inclusao_carteiras')->where($tipoInsercao,$idTipo)->update([
                'card_unimed'=>$card_unimed,
                'lead_id'=>$leadId,
                $tipoInsercao=>$idTipo,
                'retorno_unimed'=>$message,
                'proposta'=>$proposta,
                'come_id'=>$user_id,
                'data_operacao'=>date('Y-m-d H:i:s'),
                'data_inclusao_unimed'=>$dataInclusao
            ]);
        }
    }
    private function updateCard($card_unimed,$message,$proposta,$dataTitular,$dataInclusao,$user_id)
    {
        //atuliaza um novo cartão e atualiza as informacoes no titular e ou beneficiarios
        try {
            //inicia um transação
            DB::beginTransaction();
            $tipoInsercao = $dataTitular['tipo'] == "beneficiario" ? "bene_id" : "titu_id";

            $this->saveOrUpdate(
                $card_unimed,$tipoInsercao,
                $dataTitular['id_tipo'],$dataInclusao,
                $dataTitular['lead_id'],$message,
                $proposta,$user_id
            );

            //Atualuiza a tabela carteiras pelo card ID
            $cardUpdate = $this->where($tipoInsercao,$dataTitular['id_tipo'])
                ->update([
                    'card_criacao' => date("Y-m-d H:i:s"),
                    'lead_id' => $dataTitular['lead_id'],
                    //$tipoInsercao => $dataTitular['id_tipo'],
                    'card_unimed' => $card_unimed,
                    'card_pdv'=>0,
                    'card_linha_txt' => $message,
                    'card_todo'=>0,
                    'card_proposta' => $proposta,
                    'card_unimed_criacao' => $dataInclusao,
                    'card_status'=>'ativo'
                ]);
            //echo "grava atualiza cartão";

            if ($dataTitular['tipo'] == "beneficiario") {
                //atualiza a o beneficiario
                $updateBeneficiario = Beneficiario::where('bene_id', $dataTitular['id_tipo'])
                    ->update(
                        [
                            'bene_num_unimed' => $card_unimed,
                            //'card_id' => $card_unimed,
                            'uni_date_atualizacao' => $dataInclusao
                        ]);


                //echo "atualiza beneficiario";
            } else {
                //atualiza a o Titular
                $updateTitular = Titular::where('titu_id', $dataTitular['id_tipo'])
                    ->update(
                        [
                            'titu_num_unimed' => $card_unimed,
                            //'card_id' => $card_unimed,
                            'uni_date_atualizacao' => $dataInclusao
                        ]);
                //echo "atualiza titular";
                //Atualiza Lead para o caso do titular_pagador
                if ($dataTitular['tipo'] == "titular_pagador") {
                    $updateLead = Lead::where('lead_id', $dataTitular['lead_id'])
                        ->update([
                            'uni_date_criacao' => $dataInclusao,
                            //'card_id' => $card_unimed
                        ]);
                }
                //echo "atualiza lead";
            }
            //exit;
            //return true;
            DB::commit();
            return true;
        } catch (\Exception $e){
            DB::rollback();
            //var_dump($e->getMessage()); exit;
            return false;
        }
    }

    private function generateNewCard($card_unimed,$message,$proposta,$dataTitular,$dataInclusao,$user_id)
    {
        //cria um novo cartão e atualiza as informacoes no titular e ou beneficiarios
        $tipoInsercao = $dataTitular['tipo'] == "beneficiario" ? "bene_id" : "titu_id";
        //lastInsert ID do card
        $this->saveOrUpdate(
            $card_unimed,$tipoInsercao,
            $dataTitular['id_tipo'],$dataInclusao,
            $dataTitular['lead_id'],$message,
            $proposta,$user_id
        );

        $cardId = $this->create([
            'card_criacao' => date("Y-m-d H:i:s"),
            'lead_id' => $dataTitular['lead_id'],
            $tipoInsercao => $dataTitular['id_tipo'],
            'card_unimed' => $card_unimed,
            'card_pdv'=>0,
            'card_linha_txt' => $message,
            'card_todo'=>0,
            'card_proposta' => $proposta,
            'card_unimed_criacao' => $dataInclusao,
            'card_status'=>'ativo'
        ]);
        try {
            //inicia um transação
            DB::beginTransaction();
            //lastInsert ID do card
            if ($dataTitular['tipo'] == "beneficiario") {
                //atualiza a o beneficiario
                $updateBeneficiario = Beneficiario::where('bene_id', $dataTitular['id_tipo'])
                    ->update(
                        [
                            'bene_num_unimed' => $card_unimed,
                            //'card_id' => $card_unimed,
                            'uni_date_criacao' => $dataInclusao,
                            //'uni_date_exclusao' => ""
                        ]);
            } else {
                //cho "passando cartão para titular criado ".$cardId;
                $updateTitular = Titular::where('titu_id', $dataTitular['id_tipo'])
                    ->update(
                        [
                            'titu_num_unimed' => $card_unimed,
                            //'card_id' => $card_unimed,
                            'uni_date_criacao' => $dataInclusao,
                            //'uni_date_exclusao' => ""
                        ]);
                //Atualiza Lead para o caso do titular_pagador
                if ($dataTitular['tipo'] == "titular_pagador") {
                    $updateLead = Lead::where('lead_id', $dataTitular['lead_id'])
                        ->update([
                            //'card_id' => $card_unimed,
                            'uni_date_criacao' => $dataInclusao
                        ]);

                }
            }
            DB::commit();
            return true;
        } catch (\Exception $e){
            DB::rollback();
            return false;
        }
    }

    public function getCardUnimed($card_unimed,$dataOperacao,$status,$msg)
    {
        $getBeneOrTituId = function($card_unimed){
            $std = new \stdClass();
            $searchLeadTitular = Titular::where('card_id',$card_unimed)->first();
            if(!is_null($searchLeadTitular)) {
                $std->titu_id = $searchLeadTitular->titu_id;
                $std->bene_id = 0;
                $std->lead_id = $searchLeadTitular->lead_id;
                return $std;
            }
            $searchBeneficiario = Beneficiario::where('card_id',$card_unimed)->first();
            if(!is_null($searchBeneficiario)){
                $std->bene_id = $searchBeneficiario->bene_id;
                $std->titu_id = 0;
                $std->lead_id = $searchBeneficiario->lead_id;
                return $std;
            }
            return null;
        };

        $beneOrTitu = $getBeneOrTituId($card_unimed);
        //var_dump($getCarteira);
        if(is_null($beneOrTitu)) return [];
        if($beneOrTitu->bene_id!=0){
            $beneficiario=Beneficiario::where('bene_id',$beneOrTitu->bene_id)->first();
            $lead = Lead::where('lead_id',$beneficiario->lead_id)->first();
            $retornoUsuario = [
                'tipo_cadastro_agente_brasil'=> "Beneficiario",
                'tipo_cadastro_unimed'=>'Titular',
                'message'=>"",
                'carteira'=>$card_unimed,
                'proposta'=>'',
                'nome'=>$beneficiario->bene_nome,
                'lead_cpf'=>$lead->lead_cpf,
                'status'=>'',
                'data_inclusao'=>'',
                'origin'=>''
            ];
            if($status=='success'){
                Beneficiario::where('lead_id',$beneficiario->lead_id)->update(['uni_date_exclusao'=>$dataOperacao]);
                Carteira::where('card_unimed',$card_unimed)->update(['card_linha_txt'=>$msg,'card_status'=>'cancelado']);
            }
        } else {
            $titular = Titular::where('titu_id',$beneOrTitu->titu_id)->first();
            //var_dump($titular); //exit;
            $lead = Lead::where('lead_id',$beneOrTitu->lead_id)->first();
            //var_dump($lead); exit;
            $retornoUsuario = [
                'tipo_cadastro_agente_brasil'=> "Titular",
                'tipo_cadastro_unimed'=>'Titular',
                'message'=>"",
                'carteira'=>$card_unimed,
                'proposta'=>'',
                'nome'=>$titular->titu_nome,
                'lead_cpf'=>$lead->lead_cpf,
                'status'=>'',
                'data_inclusao'=>null,
                'origin'=>null
            ];
            if($status=='success') {
                Titular::where('lead_id', $titular->lead_id)->update(['uni_date_exclusao' => $dataOperacao]);
                Carteira::where('card_unimed',$card_unimed)->update(['card_linha_txt'=>$msg,'card_status'=>'cancelado']);
            }
        }
        return $retornoUsuario;
    }

    public static function getObjCardUnimedByTitularOrBeneficiario($titular)
    {
        $tipo = in_array($titular['tipo'],['titular','titular_pagador']) ? "titu_id" : "bene_id";
        $getCard = self::where($tipo,$titular['id_tipo'])->first();

        if(empty($getCard)) return null;
        $status = "Não importado";

        if($getCard->card_linha_txt!=""){
            if(strrpos($getCard->card_linha_txt,"excluído com sucesso.") == true){
                $status = "Carteira Excluída";
            } elseif(strrpos($getCard->card_linha_txt,"importada com sucesso.") == true){
                $status = "Carteira Importada";
            }
        }
        $objRetorno = [
            'status'=> $status,
            'card_unimed'=>$getCard->card_unimed
        ];

        return $objRetorno;
    }

    public static function updateLead($titular,$cardUnimed)
    {
        $tipo = in_array($titular['tipo'],['titular','titular_pagador']) ? "titu_id" : "bene_id";
        $data = self::where($tipo,$titular['id_tipo'])->first();
        //var_dump($data); exit;
        if(is_null($data)) return false;

        if($titular['tipo']=="titular_pagador"){
            $lead = Lead::where('lead_id',$data->lead_id)
                ->update([
                    'card_id'=>$cardUnimed
                ]);
        }

        if($data->titu_id != 0){
            $titular = Titular::where('titu_id',$data->titu_id)
                ->update([
                    'card_id'=>$cardUnimed
                ]);
        } else {
            $beneficiario = Beneficiario::where('bene_id',$data->bene_id)
                ->update([
                    'card_id'=>$cardUnimed
                ]);
        }
        return true;
    }


}

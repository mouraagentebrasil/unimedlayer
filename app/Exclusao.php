<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;



class Exclusao extends Model
{
    //
    protected $table = 'exclusoes';
    protected $guarded = ['exclu_id'];
    public $timestamps = false;
    //grava uma exclusao na tabela de exclusao
    public static function incluiExclusao($data,$objExcl,$user_id)
    {
        $lead = Lead::where('card_id',$data['card_unimed'])->first();
        if(is_null($lead)) return false;
        $save = self::create([
            'lead_id'=>$lead->lead_id,
            'exclu_data_exclusao'=>$data['data_formatada'],
            'card_id'=>$data['card_unimed'],
            'exclu_date'=>date('Y-m-d'),
            'exclu_obs'=>$data['observacao_exclusao'],
            'exclu_motivo'=>$data['motivo_exclusao'],
            'exclu_origem'=>$data['origem_exclusao'],
            'come_id'=>$user_id
        ]);
        if($save){
            //desativa a carteira
            DB::table('inclusao_carteiras')->where('card_unimed',$data['card_unimed'])->update(['fl_ativo'=>0]);
            return true;
        }
        return false;
    }
}

<?php

namespace App\Providers;

use Illuminate\Bus\Dispatcher;
use Illuminate\Contracts\Events\Dispatcher as Dispat;
use Illuminate\Support\ServiceProvider;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispat $events)
    {
        //Modificações no Menu
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            $user = \Session::get('user');
            $event->menu->add('BEM VINDO '.strtoupper($user[0][0]['come_nome']));
            $event->menu->add([
                'text' => 'Buscas',
                'url' => '/newsearch',
                'icon'=> 'search'
            ]);

            $event->menu->add([
                'text' => 'Gera Carteiras Unimed',
                'url' => '/criaCarteiras',
                'icon'=> 'users'
            ]);

            $event->menu->add([
                'text' => 'Operações por usuario',
                'url' => '/operacoesUsuario',
                'icon'=> 'edit'
            ]);

            $event->menu->add([
                'text' => 'Auditoria Carteiras',
                'url' => '/auditoriaCarteirasPorUsuario',
                'icon' => 'legal'
            ]);

            $event->menu->add([
                'text' => 'Exclusão Carteira',
                'url' => '/exclusaoCarteiras',
                'icon'=>'remove'
            ]);

            $event->menu->add([
                'text' => 'Auditoria Automatica',
                'url' => '/auditoriaAutomatica',
                'icon'=>'bug'
            ]);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditoriaCarteira extends Model
{
    //
    protected $guarded = ['audi_id'];
    protected $table = 'auditoria_carteira';
    public $timestamps = false;
}

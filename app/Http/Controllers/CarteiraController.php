<?php

namespace App\Http\Controllers;

use App\AuditoriaCarteira;
use App\Carteira;
use App\Exclusao;
use App\Exclusoes;
use App\Titular;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Lead;
use App\ServiceXml\UnimedGeraPropostaXmlService as PropostaUnimed;
use App\ServiceXml\UnimedExclusaoCarteiraXmlService as ExclusaoUnimed;
use GuzzleHttp\Client;

class CarteiraController extends Controller
{
    //
    protected $proposta;

    public function geraCarteiras(Request $request)
    {
        $retorno = [];
        $prevalider = [];
        $user = $request->session()->get('user')[0][0];
        $data = $request->all();
        $leadInfo = [];
        $lead = new Lead;
        $leadInfo = $lead->getInfoLeads($data['cpfs']);
        if(empty($leadInfo[0])) return response()->json(['message'=>'Nenhum lead encontrado'],400);
        $beneficiarios = $lead->getBeneficiarios($leadInfo);
        $titulares = $lead->getTitulares($leadInfo);
        $dateFormat = \DateTime::createFromFormat('d/m/Y',$data['dataCriacao']);
        $data['dataCriacaoFormatado'] = $dateFormat->format('Y-m-d');

        for($i=0;$i<count($leadInfo);$i++){
            $proposta = new PropostaUnimed(
                $leadInfo[$i],$titulares[$i],$beneficiarios[$i],$data['dataCriacaoFormatado']
            );
            $prevalider = $this->preValider($proposta,$user);
            if($prevalider['status']==true){
                $retorno[$i] = $prevalider['retorno'];
            } else {
                $retorno[$i] = $this->postSoap($proposta,$user);
            }
        }
        return response()->json($retorno,200);
    }

    public function postSoap($proposta,$user)
    {
        $response = [];
        $url = getenv("WEB_SERVER_UNIMED");
        $client = new Client();
        $xml = $proposta->processaXmlSoapUnimedGeraProposta();
        $dadosTitulares  = $proposta->getTitularesMerged();
        //var_dump($dadosTitulares); exit;
        for($i=0;$i<count($xml);$i++):
            $r = $client->request('POST', $url, [
                'headers'=> [
                    'Content-Type' => 'text/xml; charset=UTF8'
                ],
                'body' => $xml[$i]
            ]);
            $response[$i] = $this->processSoapResponse(
                new \GreenCape\Xml\Converter($r->getBody()->getContents()),
                $dadosTitulares[$i],
                $user,
                $proposta->data_inclusao
            );
        endfor;

        return $response;
    }

    public function processSoapResponse($soapResp,$titular,$user,$dataInclusao)
    {
        $getPropostaId = function($mensagem) {
            $propostapp = $mensagem;
            $propostapp=str_replace('Proposta ','',$propostapp);
            $propostapp=str_replace('importada com sucesso.','',$propostapp);
            return $propostapp;
        };
        $status = true;
        $resp = $soapResp->data;
        $arrayBase = $resp["soap:Envelope"]["soap:Body"]["ImportarPropostaPfResponse"]["ImportarPropostaPfResult"];
        $mensagem = $arrayBase[0]['Mensagem'];
        //var_dump($mensagem); exit;
        if(strrpos($mensagem,"importada com sucesso.")==false) $status = false;
        //var_dump($status);
        if($status){
            $propostaId = $getPropostaId($mensagem);
            $contrato = $arrayBase[1]['Contrato'];
            $card_unimed = $contrato[2]['Beneficiarios'][0]['NumeroAssociado'];
            $nomeBeneficiario = $contrato[2]['Beneficiarios'][1]['Nome'];
            //grava log
            file_put_contents(
                storage_path('logs/respostaUnimed/'.date('Y-m-d').'_importacao_lead_'.$titular['lead_id']."_ID_".time().rand(1,200).'.txt'),
                var_export($resp,true)
            );

            $carteira = new Carteira();
            //testa se foi importado com sucesso
            $salvaCarteira = $carteira->insertOrUpdateCarteira($card_unimed,$mensagem,$propostaId,$titular,$dataInclusao,$user['come_id']);
            //var_dump($salvaCarteira); exit;
            if($salvaCarteira){

                Carteira::updateLead($titular,$card_unimed);
                //die('oi eu sou goku');
                $retornoUsuario = [
                    'tipo_cadastro_agente_brasil'=> $titular['tipo']=="titular_pagador" ? "Titular Pagador" : ucfirst($titular['tipo']),
                    'tipo_cadastro_unimed'=>'Titular',
                    'message'=>$mensagem,
                    'carteira'=>$card_unimed,
                    'proposta'=>$propostaId,
                    'nome'=>$nomeBeneficiario,
                    'lead_cpf'=>$titular['lead_cpf'],
                    'status'=>'success',
                    'data_inclusao'=>$dataInclusao
                ];
                //grava auditoria sucesso
                AuditoriaCarteira::create([
                    'come_id'=>$user['come_id'],
                    'lead_id'=>$titular['lead_id'],
                    'obj_retorno'=>json_encode($retornoUsuario),
                    'data_operacao'=>date('Y-m-d H:i:s')
                ]);
                return $retornoUsuario;
            }
        }
        //em caso de erro
        $retornoUsuario = [
            'tipo_cadastro_agente_brasil'=> $titular['tipo']=="titular_pagador" ? "Titular Pagador" : ucfirst($titular['tipo']),
            'tipo_cadastro_unimed'=>'Titular',
            'message'=>$mensagem,
            'carteira'=>null,
            'proposta'=>null,
            'nome'=>$titular['nome'],
            'lead_cpf'=>$titular['lead_cpf'],
            'status'=>'error',
            'data_inclusao'=>null,
            'origin'=>null
        ];
        Lead::where('lead_id',$titular['lead_id'])->update(['uni_error'=>$mensagem]);
        //grava auditoria error
        AuditoriaCarteira::create([
            'come_id'=>$user['come_id'],
            'lead_id'=>$titular['lead_id'],
            'obj_retorno'=>json_encode($retornoUsuario),
            'data_operacao'=>date('Y-m-d H:i:s')
        ]);
        return $retornoUsuario;
    }

    //regras de validação dos leads,titulares e beneficiarios
    public function preValider($proposta,$user)
    {
        $status = false;
        $this->proposta = $proposta;
        $titulares = $this->proposta->getTitularesMerged();
        $retorno = [];
        $return = ['retorno'=>null,'status'=>false];

        for($i=0;$i<count($titulares); $i++){
            $retorno[$i] = [
                'tipo_cadastro_agente_brasil'=> $titulares[$i]['tipo']=="titular_pagador" ? "Titular Pagador" : ucfirst($titulares[$i]['tipo']),
                'tipo_cadastro_unimed'=>'Titular',
                'nome'=>$titulares[$i]['nome'],
                'lead_cpf'=>$titulares[$i]['lead_cpf'],
                'carteira'=>null,
                'proposta'=>null,
                'data_inclusao'=>null,
                'status'=>null,
                'message'=>'',
                'origem'=>null
            ];

            if(Lead::where('lead_id',$titulares[$i]['lead_id'])->whereIn('lead_status',['cancelado','cancelado pagamento','cancelado duplicado'])->first() != null){
                $retorno[$i]['message'] .= " | Lead Cancelado | ";
                $status = true;
            }
            //testa se o lead tem mais de um titular na base agente brasil
            if(Titular::where('lead_id',$titulares[$i]['lead_id'])->count() > 1){
                $retorno[$i]['message'] .= " | Lead com mais de um Titular | ";
                $status = true;
            }
            //testa se o lead é menor de idade
            if($this->proposta->calculaIdade(Lead::getDataNascimentoLead($titulares[$i]['lead_id'])) < 18){
                $retorno[$i]['message'] .= " | Lead ID não pode ser menor de idade | ";
                $status = true;
            }

            if($titulares[$i]['tipo']=="titular" || $titulares[$i]['tipo']=="titular_pagador" ){
                //testa se o titular ou beneficiario é maior e se for maior testa se falta informações
                if($this->proposta->calculaIdade($titulares[$i]['nascimento']) > 18) {
                    if ($titulares[$i]['cpf'] == "") {
                        $retorno[$i]['message'] .= " | CPF em branco | ";
                        $status = true;
                    }
                }
            }
            //Essa verificação é redundante, mas pode acontecer
            if($titulares[$i]['tipo']=="titular_pagador" && $this->proposta->calculaIdade($titulares[$i]['nascimento']) < 18){
                $retorno[$i]['message'] .= " | Titular pagador não pode ser menor de idade | ";
                $status = true;
            }

            if($titulares[$i]['logradouro'] == "") {
                $retorno[$i]['message'] .= " | Logradouro em branco | ";
                $status = true;
            }

            if($titulares[$i]['nascimento'] == "") {
                $retorno[$i]['message'] .= " | Nascimento em branco | ";
                $status = true;
            }
            if($titulares[$i]['tipo']=="titular" && $titulares[$i]['cpf'] == $titulares[$i]['lead_cpf']){
                $retorno[$i]['message'] .= " | Titular com mesmo CPF do LEAD somente pagador | ";
                $status = true;
            }

            if($titulares[$i]['tipo']=="benificiario" && $titulares[$i]['cpf'] == $titulares[$i]['lead_cpf']){
                $retorno[$i]['message'] .= " | Beneficiario com mesmo CPF do LEAD | ";
                $status = true;
            }
            //grava auditoria caso o status seja true (ou seja errado)
            if($status==true){
                AuditoriaCarteira::create([
                    'come_id'=>$user['come_id'],
                    'lead_id'=>$titulares[$i]['lead_id'],
                    'obj_retorno'=>json_encode($retorno[$i]),
                    'data_operacao'=>date('Y-m-d H:i:s')
                ]);
            }
        }
        $return['retorno'] = $retorno;
        $return['status']=$status;
        return $return;
    }

    public function exclusaoCarteira(Request $request)
    {
        $user = $user = $request->session()->get('user')[0][0];
        $data = $request->all();

        $dateFormat = \DateTime::createFromFormat('d/m/Y',$data['data_exclusao']);
        $data['data_formatada'] = $dateFormat->format('Y-m-d');

        $objCardUnimedExclusao = [
            'tipo_cadastro_agente_brasil'=> null,
            'tipo_cadastro_unimed'=>'Titular',
            'message'=>"",
            'carteira'=>$data['card_unimed'],
            'proposta'=>null,
            'nome'=>null,
            'lead_cpf'=>null,
            'status'=>null,
            'data_inclusao'=>$data['data_formatada'],
            'origin'=>null
        ];

        $exclusaoUnimed = new ExclusaoUnimed(
            $data['data_exclusao'],
            $data['card_unimed'],
            $data['observacao_exclusao'],
            $data['motivo_exclusao']
        );
        $carteira = new Carteira();

        $postExclusaoUnimed = function($exclusaoUnimed) {
            $client = new Client();
            $url = getenv("WEB_SERVER_UNIMED");
            $r = $client->request('POST', $url, [
                'headers'=> [
                    'Content-Type' => 'text/xml; charset=UTF8'
                ],
                'body' => $exclusaoUnimed->getXmlTemplateExclusaoBeneficiario()
            ]);

            $xmlResponse = new \GreenCape\Xml\Converter($r->getBody()->getContents());
            return $xmlResponse->data;
        };

        $dataPost = $postExclusaoUnimed($exclusaoUnimed);
        $baseArray = $dataPost["soap:Envelope"]["soap:Body"]["ExclusaoBeneficiarioResponse"];

        if(isset($baseArray['ERROS'])){
            $status = 'error';
            if(
                isset($baseArray['ERROS']['Mensagem']) &&
                strrpos($baseArray['ERROS']['Mensagem'],"com sucesso")==true
            ){
                $msg = $baseArray['ERROS']['Mensagem'];
                $status = 'success';
                //atualiza informações nas tabelas de lead, titular ou beneficiario
                $objCardExclude = $carteira->getCardUnimed($data['card_unimed'],$data['data_formatada'],$status,$msg);
                $objCardUnimedExclusao = !empty($objCardExclude) ? $objCardExclude : $objCardUnimedExclusao;
                //insere essa informação na tabela exclusões
                Exclusao::incluiExclusao($data,$objCardExclude,$user['come_id']);
            } else {

                if(isset($baseArray['ERROS']['Mensagem'])){
                    $msg = $baseArray['ERROS']['Mensagem'];
                } else {
                    $msg =
                        $baseArray['ERROS'][0]['Mensagem'].
                        " | COD_MENSAGEM: ".$baseArray['ERROS'][1]['Erros'][0]['COD_MENSAGEM'].
                        " | TXT_MENSAGEM: ".$baseArray['ERROS'][1]['Erros'][2]['TXT_MENSAGEM'].
                        " | ".$baseArray['ERROS'][1]['Erros'][3]['TXT_COMPLEMENTO_MSG'];
                }

                $objCardExclude = $carteira->getCardUnimed($data['card_unimed'],$data['data_formatada'],$status,$msg);
                $objCardUnimedExclusao = !empty($objCardExclude) ? $objCardExclude : $objCardUnimedExclusao;
            }

            $objCardUnimedExclusao['message'] = $msg;
            $objCardUnimedExclusao['status'] = $status;
            $objCardUnimedExclusao['data_inclusao'] = $data['data_formatada'];

            AuditoriaCarteira::create([
                'come_id'=>$user['come_id'],
                'obj_retorno'=>json_encode($objCardUnimedExclusao),
                'data_operacao'=>date('Y-m-d H:i:s')
            ]);
        }
        return response()->json($objCardUnimedExclusao,200);
    }
}

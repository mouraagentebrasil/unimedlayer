<?php
/**
 * Created by PhpStorm.
 * User: felipemoura
 * Date: 14/12/2016
 * Time: 10:01
 */

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Symfony\Component\DomCrawler\Crawler;

class GatewayBoletos extends Controller
{
    public $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getInfoLeadPlan($lead_id)
    {
        $lead = DB::table('leads')->where('lead_id',$lead_id)->first();
        $plano = DB::table('planos')->where('plan_logica',$lead->plan_id)->first();

        $return['plan'] = $plano;
        $return['lead'] = $lead;
        return $return;
    }

    public function getLinhaDigitavel($cobranca)
    {
        $cobranca = DB::table('cobrancas')->where('cod',$cobranca)->first();
        if(is_null($cobranca)) return ['linha_digitavel'=>null,'valor'=>0.00,'data_vencimento'=>null];
        return [
            'linha_digitavel'=>$cobranca->linha_digitavel,
            'valor'=>$cobranca->valor,
            'data_vencimento'=>$cobranca->vencimento
        ];
    }
}
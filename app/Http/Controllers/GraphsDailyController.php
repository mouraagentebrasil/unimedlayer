<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class GraphsDailyController extends Controller
{
    //
    public $product = [];

    public function __construct()
    {
        $this->product = [
            'pet'=>[
                'stats'=>self::queryBuilderStatsPet(),
                'leads'=>self::queryBuilderPetLeads()
            ],
            'metlife'=>[
                'stats'=>self::queryBuilderStatsMetLife(),
                'leads'=>self::queryBuilderMetlifeLeads()
            ],
            'unimed'=>[
                'stats'=>self::queryBuilderStatsUnimed(),
                'leads'=>self::queryBuilderLeadUnimed()
            ]
        ];
    }



    public static function queryBuilderPetLeads() {
        return DB::table('pet_leads');
    }

    public static function queryBuilderMetlifeLeads() {
        return DB::table('metlife_leads');
    }

    public static function queryBuilderStatsPet() {
        return DB::table('stats_pet');
    }

    public static function queryBuilderStatsMetLife() {
        return DB::table('stats_metlife');
    }

    public static function queryBuilderStatsUnimed() {
        return DB::table('stats_lp');
    }

    public static function queryBuilderLeadUnimed() {
        return DB::table('leads');
    }

    public function getObjectGraphStatsDate(Request $request,$product,$date){
        $arrRet = [];
        $arrRet['stats_unique'] = $this->getStatsDateHoursUnique($product,$date);
        $arrRet['stats_total'] = $this->getStatsPerDate($product,$date);
        $arrRet['stats_in_hours_total'] = $this->getStatsPerHours($product,$date);
        $arrRet['stats_in_hours_unique'] = $this->getStatsUniquePerHours($product,$date);
        $arrRet['stats'] = $this->getStatsStats($product,$date);
        return response()->json($arrRet,200);
    }
    //stats per date
    public function getStatsDateHoursUnique($product,$date = null){
        $data = $this->product[$product]['stats']->distinct()->select('lp_ip');
        if(!is_null($date)) {
            $data->where('lp_date','>=',$date." 00:00:00")
                ->where('lp_date','<=',$date." 23:59:59");
        }
        return count($data->get());
    }
    //stats per date
    public function getStatsPerDate($product,$date = null){
        $data = $this->product[$product]['stats'];
        if(!is_null($date)) {
            $data->where('lp_date','>=',$date." 00:00:00");
            $data->where('lp_date','<=',$date." 23:59:59");
        }
        return $data->count();
    }
    //stats per date
    public function getStatsPerHours($product,$date){
        $data = $this->product[$product]['stats'];
        $data->select('lp_date');
        $data->where('lp_date','>=',$date." 00:00:00");
        $data->where('lp_date','<=',$date." 23:59:59");

        $getDateIn24h = function($date) use ($data){
            $get = $data->get();
            $return = [];
            for($h=0;$h<24;$h++){$return[$h]=0;}
            for($h=0;$h<24;$h++){
                for($i=0;$i<count($get);$i++){
                    if( $this->mkTime($get[$i]->lp_date) >= $this->mkTime($date." ".$h.":00:00")
                        &&
                        $this->mkTime($get[$i]->lp_date) <= $this->mkTime($date." ".$h.":59:59") ){
                        $return[$h] = isset($return[$h]) ? $return[$h]+1: 1;
                    }
                }
            }
            return $return;
        };
        return $getDateIn24h($date);
    }
    //stats per date
    public function getStatsUniquePerHours($product,$date){
        $dateI = $date." 00:00:00";
        $dateF = $date." 23:59:59";

        $result = DB::select(
            'call getStatsUnicos(?,?,?)',
            [$dateI,$dateF,($product == "unimed" ? "stats_lp": "stats_".$product)]
        );

        $getDateIn24h = function($date) use ($result,$product){
            $return = [];
            for($h=0;$h<24;$h++){$return[$h]=0;}
            for($h=0;$h<24;$h++){
                for($i=0;$i<count($result);$i++){
                    if( $this->mkTime($result[$i]->lp_date) >= $this->mkTime($date." ".$h.":00:00")
                        &&
                        $this->mkTime($result[$i]->lp_date) <= $this->mkTime($date." ".$h.":59:59") ){
                        $return[$h] = isset($return[$h]) ? $return[$h]+1: 1;
                    }
                }
            }
            return $return;
        };
        return $getDateIn24h($date);
    }

    public function getStatsPerYear($product,$year)
    {
        $data = $this->product[$product]['stats'];

        if(!is_null($year)) {
            $data->where('lp_date','>=',$year."-01-01");
            $data->where('lp_date','<=',$year."-12-31");
        }
        $dados = $data->get();
        $monthsInYears = function($year) use ($dados){
            $get = $dados;
            $return = [];
            for($m=1;$m<=12;$m++){$return[$m]['qtd'] = 0; $return[$m]['m'] = $this->HuminizeDateMonth($m); }
            for($m=1;$m<=12;$m++){
                for($i=0;$i<count($get);$i++){
                    if( $this->mkTime($get[$i]->lp_date) >= $this->mkTime($year."-".$m."-01 00:00:00")
                        &&
                        $this->mkTime($get[$i]->lp_date) <= $this->mkTime($year."-".$m."-".$this->lastDateOfMonth($m,$year)." 23:59:59") ){
                        $return[$m]['qtd'] = $return[$m]['qtd']+1;
                    }
                }
            }
            return $return;
        };
        $result = $monthsInYears($year);
        $return = $result;
        $return['stats'] = [];
        $avg = 0;
        for($i=0;$i<count($dados);$i++){
            $avg += $this->mkTime($dados[$i]->lp_date);
        }
        $avg = $avg != 0 ? $avg/count($dados) : null;
        $avg = $avg != null ? date('Y-m-d H:i:s',$avg) : null;

        $return['stats']['avg'] = $avg;
        $return['stats']['max'] = $dados->max('lp_date');
        $return['stats']['min'] = $dados->min('lp_date');
        return response()->json($return,200);
    }

    public function getStatsDayInRange($product,$dateI,$dateF = null)
    {
        $data = $this->product[$product]['stats'];
        $data->where('lp_date','>=',$dateI." 00:00:00");
        if(!is_null($dateF)) { $data->where('lp_date','<=',$dateF." 23:59:59"); } else {
            $dateF = date('Y-m-d',strtotime("+7 days",strtotime($dateI)));
            $data->where('lp_date','<=',$dateF." 23:59:59");
        }

        $diff = $this->diffDates($dateI,$dateF)+1;
        $dados = $data->get();
        $getStatsDayInRange = function($diff) use ($dados,$dateI){
            $get = $dados;
            $return = [];
            for($d=1;$d<=$diff;$d++){
                $date_ant = date('Y-m-d',$this->mkTime($dateI." 00:00:00")-(24*3600));
                $strDateFuture = (string) "+".$d." days";
                $data_comp = date('Y-m-d',strtotime($strDateFuture,strtotime($date_ant)));

                $return[$d]['qtd'] = 0;
                $return[$d]['w'] = $this->HumanizeDateWeek($data_comp);
                $return[$d]['d'] = $data_comp;
            }
            //die("oi");
            for($d=1;$d<=$diff;$d++){
                $date_ant = date('Y-m-d',$this->mkTime($dateI." 00:00:00")-(24*3600));
                $strDateFuture = (string) "+".$d." days";
                $data_comp = date('Y-m-d',strtotime($strDateFuture,strtotime($date_ant)));
                for($i=0;$i<count($get);$i++){
                    //var_dump($get[10625]); exit;
                    if( $this->mkTime($get[$i]->lp_date) >= $this->mkTime($data_comp." 00:00:00")
                        &&
                        $this->mkTime($get[$i]->lp_date) <= $this->mkTime($data_comp." 23:59:59") ){
                        $return[$d]['qtd'] = $return[$d]['qtd']+1;
                    }
                }
            }
            return $return;
        };

        $result = $getStatsDayInRange($diff);
        $return = $result;
        $return['stats'] = [];
        $avg = 0;
        for($i=0;$i<count($dados);$i++){
            $avg += $this->mkTime($dados[$i]->lp_date);
        }
        $avg = $avg != 0 ? $avg/count($dados) : null;
        $avg = $avg != null ? date('Y-m-d H:i:s',$avg) : null;

        $return['stats']['avg'] = $avg;
        $return['stats']['max'] = $dados->max('lp_date');
        $return['stats']['min'] = $dados->min('lp_date');
        return response()->json($return,200);
    }

    public function getStatsDayInRangeByCampaing($product, $campaing, $dateI, $dateF = null)
    {
        $data = $this->product[$product]['stats'];
        $data->where('lp_url','like','%lp='.$campaing.'%');
        $data->where('lp_date','>=',$dateI." 00:00:00");
        if(!is_null($dateF)) { $data->where('lp_date','<=',$dateF." 23:59:59"); } else {
            $dateF = date('Y-m-d',strtotime("+7 days",strtotime($dateI)));
            $data->where('lp_date','<=',$dateF." 23:59:59");
        }
        $diff = $this->diffDates($dateI,$dateF) +1;
        $dados = $data->get();
        $getStatsDayInRange = function($diff) use ($dados,$dateI){
            //var_dump($data->toSql()); exit;
            $get = $dados;
            $return = [];
            for($d=1;$d<=$diff;$d++){
                $date_ant = date('Y-m-d',$this->mkTime($dateI." 00:00:00")-(24*3600));
                $strDateFuture = (string) "+".$d." days";
                $data_comp = date('Y-m-d',strtotime($strDateFuture,strtotime($date_ant)));

                $return[$d]['qtd'] = 0;
                $return[$d]['w'] = $this->HumanizeDateWeek($data_comp);
                $return[$d]['d'] = $data_comp;
            }
            for($d=1;$d<=$diff;$d++){
                $date_ant = date('Y-m-d',$this->mkTime($dateI." 00:00:00")-(24*3600));
                $strDateFuture = (string) "+".$d." days";
                $data_comp = date('Y-m-d',strtotime($strDateFuture,strtotime($date_ant)));
                for($i=0;$i<count($get);$i++){
                    if( $this->mkTime($get[$i]->lp_date) >= $this->mkTime($data_comp." 00:00:00")
                        &&
                        $this->mkTime($get[$i]->lp_date) <= $this->mkTime($data_comp." 23:59:59") ){
                        $return[$d]['qtd'] = $return[$d]['qtd']+1;
                    }
                }
            }
            return $return;
        };
        $result = $getStatsDayInRange($diff);
        $return = $result;
        $return['stats'] = [];
        $avg = 0;
        for($i=0;$i<count($dados);$i++){
            $avg += $this->mkTime($dados[$i]->lp_date);
        }
        $avg = $avg != 0 ? $avg/count($dados) : null;
        $avg = $avg != null ? date('Y-m-d H:i:s',$avg) : null;

        $return['stats']['avg'] = $avg;
        $return['stats']['max'] = $dados->max('lp_date');
        $return['stats']['min'] = $dados->min('lp_date');
        return response()->json($return,200);
    }

    public function getStatsCampaing($product)
    {
        $mapCorrect = function($col1){
            $newString = explode('?lp=',$col1);
            return $newString[1];
        };
        $data = $this->product[$product]['stats'];
        $data->select('lp_url');
        $data->selectRaw('count(*) as quant');
        $data->where('lp_url','like','%lp=%');
        $data->groupBy('lp_url');
        $dados = $data->get();

        $statsStats = function () use ($dados){
            $return = [];
            $return['avg'] = $dados->avg('quant');
            $return['max'] = $dados->max('quant');
            $return['min'] = $dados->min('quant');
            return $return;
        };
        $return = $dados;

        for ($i=0;$i<count($dados);$i++){
            $return[$i]->name = $mapCorrect($dados[$i]->lp_url);
        }

        $return['stats'] = $statsStats();
        return response()->json($return,200);
    }

    public function getCampaing($product){
        $data = $this->product[$product]['leads'];
        $resp = $data->select('lead_origem')->where('lead_origem','<>','')->distinct()->get();
        $return = [];
        foreach ($resp as $item):
            $return[str_slug($item->lead_origem)] = $item->lead_origem;
        endforeach;
        return $return;
    }

    public function getStatsStats($product,$date)
    {
        $return = [];
        $data = $this->product[$product]['stats'];
        $data->select(['lp_date','lp_ip']);
        $data->where('lp_date','>=',$date." 00:00:00");
        $data->where('lp_date','<=',$date." 23:59:59");
        $dados = $data->get()->toArray();
        $avg = 0;
        for($i=0;$i<count($dados);$i++){
            $avg += $this->mkTime($dados[$i]->lp_date);
        }
        $avg = $avg != 0 ? $avg/count($dados) : null;
        $avg = $avg != null ? date('Y-m-d H:i:s',$avg) : null;

        $return['avg'] = $avg;
        $return['max'] = $data->max('lp_date');
        $return['min'] = $data->min('lp_date');
        return $return;
    }

    public function getStatsPerMonths($product,$monthYear)
    {
        $data = $this->product[$product]['stats'];
        list($year,$month) = explode("-",$monthYear);
        $lastDay = $this->lastDateOfMonth($month,$year);
        $dateI = $monthYear."-01 00:00:00";
        $dateF = $monthYear."-".$lastDay." 23:59:59";
        $data->where('lp_date','>=',$dateI)
            ->where('lp_date','<=',$dateF);
        $dados = $data->get();
        $daysStatsInMonths = function($lastDay,$dados) use ($monthYear){
            $get = $dados;
            $return = [];
            for($d=1;$d<=$lastDay;$d++){$return[$d]['qtd'] = 0; $return[$d]['w'] = $this->HumanizeDateWeek($monthYear."-".$d); }
            for($d=1;$d<=$lastDay;$d++){
                for($i=0;$i<count($get);$i++){
                    if( $this->mkTime($get[$i]->lp_date) >= $this->mkTime($monthYear."-".$d." 00:00:00")
                        &&
                        $this->mkTime($get[$i]->lp_date) <= $this->mkTime($monthYear."-".$d." 23:59:59") ){
                        $return[$d]['qtd'] = $return[$d]['qtd']+1;
                    }
                }
            }
            return $return;
        };
        $result = $daysStatsInMonths($lastDay,$dados);
        $return = $result;
        $return['stats'] = [];
        $avg = 0;
        for($i=0;$i<count($dados);$i++){
            $avg += $this->mkTime($dados[$i]->lp_date);
        }
        $avg = $avg != 0 ? $avg/count($dados) : null;
        $avg = $avg != null ? date('Y-m-d H:i:s',$avg) : null;

        $return['stats']['avg'] = $avg;
        $return['stats']['max'] = $dados->max('lp_date');
        $return['stats']['min'] = $dados->min('lp_date');
        return response()->json($return,200);

    }

    public function diffDates($date1,$date2){
        $time_inicial = strtotime($date1);
        $time_final = strtotime($date2);
        $diferenca = $time_final - $time_inicial;
        $dias = (int)floor( $diferenca / (60 * 60 * 24));
        return $dias;
    }

    public function mkTime($date){
        $exp = explode("-",$date);
        $exp2 = explode(" ",$exp[2]);
        $exp3 = explode(":",$exp2[1]);
        $dia = $exp2[0]; $mes=$exp[1]; $ano = $exp[0];
        $hora = $exp3[0]; $minuto = $exp3[1]; $segundo = $exp3[2];
        return mktime($hora,$minuto,$segundo,$mes,$dia,$ano);
    }

    public function lastDateOfMonth($month,$year)
    {
        return date("t", mktime(0,0,0,$month,'01',$year));
    }

    public function HumanizeDateWeek($date)
    {
        $diasemana = array('Domingo',
            'Segunda',
            'Terça',
            'Quarta',
            'Quinta',
            'Sexta',
            'Sabado'
        );
        $diasemana_numero = date('w', strtotime($date));
        return $diasemana[$diasemana_numero];
    }

    public function HuminizeDateMonth($pos){

        $meses = array(
            1 => 'Janeiro',
            'Fevereiro',
            'Março',
            'Abril',
            'Maio',
            'Junho',
            'Julho',
            'Agosto',
            'Setembro',
            'Outubro',
            'Novembro',
            'Dezembro'
        );

        return $meses[$pos];
    }



}

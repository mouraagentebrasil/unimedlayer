<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Comercial;

class AuthController extends Controller
{
    //
    public function login(){
        return view('spa.login');
    }
    public function auth(Request $request)
    {
        $data = $request->all();
        if(empty($data['login']) || empty($data['pass']) || empty($data)) {
            $request->session()->flash('message','Preencha Login e Senha');
            return redirect('/login');
        }
        $findLogin = Comercial::findLoginValid($data['login'],$data['pass'])->toArray();
        if(!$findLogin){
            $request->session()->flash('message','Usuario não encontrado');
            return redirect('/login');
        }

        //verifica se o usuario está fazendo um novo login
        if($this->checkSession($request)){
           return redirect('/newsearch');
        }

        unset($findLogin['come_pass']);
        $request->session()->push('user',$findLogin);
        return redirect('/newsearch');
    }

    public function logout(Request $request)
    {
        try {
            $request->session()->forget('user');
            return redirect('/login');
        } catch (\Exception $e){
            throw $e;
            return redirect('/login');
        }
    }

    public function checkSession(Request $request)
    {
        $data = $request->all();
        $user = $request->session()->get('user');
        if(!empty($user)){
            //var_dump($user); exit;
            return ($user[0][0]['come_email']==$data['login']) ? true : false;
        }
        return false;
    }

}

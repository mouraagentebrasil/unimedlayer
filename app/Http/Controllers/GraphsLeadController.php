<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;

class GraphsLeadController extends Controller
{
    //
    protected $graphsDaily = null;
    protected $dateI = null;
    protected $dateF = null;
    protected $campaing = null;
    protected $product = null;
    protected $builder = null;


    public function __construct(Request $request)
    {
        $this->graphsDaily = new \App\Http\Controllers\GraphsDailyController();
    }

    public function setData($dateI,$dateF,$campaing,$product)
    {
        $this->dateI = $dateI;
        $this->dateF = $dateF;
        $this->campaing = $campaing;
        $this->product = $product;
        $this->builder = $this->graphsDaily->product[$this->product];
    }
    public function getObjectGraphLeadQuantityRange(Request $request,$product,$campaing = null,$dateI,$dateF = null ){
        $arrRet = [];
        $this->setData($dateI,$dateF,$campaing,$product);
        var_dump($this->builder['leads']->get());
//        $arrRet['stats_unique'] = $this->getStatsDateHoursUnique($product,$dateI);
//        $arrRet['stats_total'] = $this->getStatsPerDate($product,$date);
//        $arrRet['stats_in_hours_total'] = $this->getStatsPerHours($product,$date);
//        $arrRet['stats_in_hours_unique'] = $this->getStatsUniquePerHours($product,$date);
//        $arrRet['stats'] = $this->getStatsStats($product,$date);
        return response()->json($arrRet,200);
    }

    public function getLeadsDailyRange(Request $request,$product,$campaing = null,$dateI,$dateF = null )
    {
        $this->setData($dateI,$dateF,$campaing,$product);

        if(is_null($dateF)) {
            $dateF = !is_null($this->dateF) ? $this->dateF : $this->dateI;
            $dateF = date('Y-m-d', strtotime("+7 days", strtotime($dateF)));
        }
        $allCampaing = function(){
           return $this->graphsDaily->getCampaing($this->product);
        };
        //var_dump($allCampaing()); exit;
        $humanizeCampaing=$allCampaing()[$campaing];
        //var_dump($humanizeCampaing); exit;
        //TODO problema de funçoes encadeadas no builder
        $data = DB::table($this->product == "unimed" ? "leads" : $this->product.'_leads')
            ->where('lead_origem',$humanizeCampaing)
            ->where('lead_criacao','>=',$dateI." 00:00:00")
            ->where('lead_criacao','<=',$dateF." 23:59:59")->get();
        //var_dump(count($data)); exit;
        $diff = $this->graphsDaily->diffDates($dateI,$dateF)+1;

        $getLeadsDailyInRange = function($diff) use ($data,$dateI){
            $return = [];
            //inicializa o retorno
            for($d=1;$d<=$diff;$d++){
                $date_ant = date('Y-m-d',$this->graphsDaily->mkTime($dateI." 00:00:00")-(24*3600));
                $strDateFuture = (string) "+".$d." days";
                $data_comp = date('Y-m-d',strtotime($strDateFuture,strtotime($date_ant)));

                $return[$d]['qtd'] = 0;
                $return[$d]['w'] = $this->graphsDaily->HumanizeDateWeek($data_comp);
                $return[$d]['d'] = $data_comp;
            }
            for($d=1;$d<=$diff;$d++){
                $date_ant = date('Y-m-d',$this->graphsDaily->mkTime($dateI." 00:00:00")-(24*3600));
                $strDateFuture = (string) "+".$d." days";
                $data_comp = date('Y-m-d',strtotime($strDateFuture,strtotime($date_ant)));
                for($i=0;$i<count($data);$i++){
                    if( $this->graphsDaily->mkTime($data[$i]->lead_criacao) >= $this->graphsDaily->mkTime($data_comp." 00:00:00")
                        &&
                        $this->graphsDaily->mkTime($data[$i]->lead_criacao) <= $this->graphsDaily->mkTime($data_comp." 23:59:59") ){
                        $return[$d]['qtd'] = $return[$d]['qtd']+1;
                    }
                }
            }
            return $return;
        };

        return $getLeadsDailyInRange($diff);
    }

    public function getDailyLeadsPerDate(Request $request,$product,$date)
    {
        $this->setData($date,null,null,$product);
        $data = $this->builder['leads']
            ->where('lead_criacao','>=',$date." 00:00:00")
            ->where('lead_criacao','<=',$date." 23:59:59")
            ->get();
        $getDateIn24h = function($date) use ($data){
            $return = [];
            for($h=0;$h<24;$h++){$return[$h]=0;}
            for($h=0;$h<24;$h++){
                for($i=0;$i<count($data);$i++){
                    if( $this->graphsDaily->mkTime($data[$i]->lead_criacao) >= $this->graphsDaily->mkTime($date." ".$h.":00:00")
                        &&
                        $this->graphsDaily->mkTime($data[$i]->lead_criacao) <= $this->graphsDaily->mkTime($date." ".$h.":59:59") ){
                        $return[$h] = isset($return[$h]) ? $return[$h]+1: 1;
                    }
                }
            }
            return $return;
        };
        return $getDateIn24h($date);
    }

    public function getLeadsConvertions(Request $request,$product,$campaing = null,$dateI,$dateF = null )
    {
        $table_lead = ($product == "unimed" ? "leads" : $product."_leads");
        $table_stats = ($product == "unimed" ? "stats_lp" : "stats_".$product);
        $data = DB::table($table_lead)
            ->select([$table_lead.'.lead_origem',$table_lead.'.lead_ip'])
            ->join(
                $table_stats,
                $table_stats.'.lp_ip','=',$table_lead.'.lead_ip'
            )
            ->where($table_lead.'.lead_origem',$campaing)
            //->having($table_lead.'lead_ip','!=',null)
            ->groupBy($table_lead.'.lead_nome')
            ->get()->toArray();

        var_dump($data); exit;
    }
}

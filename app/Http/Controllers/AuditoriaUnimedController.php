<?php

namespace App\Http\Controllers;

use App\AuditoriaCarteira;
use Illuminate\Http\Request;

use App\Http\Requests;

class AuditoriaUnimedController extends Controller
{
    //
    public function getAuditoriasByUser(Request $request,$data_inicial = null,$data_final = null)
    {
        $user = $request->session()->get('user')[0][0];

        $query = AuditoriaCarteira::where('come_id',$user['come_id']);

        if(!is_null($data_inicial)){
            $query->where('data_operacao',">=",$data_inicial);
        }
        else if(!is_null($data_final)) {
            $query->where('data_operacao', "<=", $data_final);
        } else {
            $query->orderBy('data_operacao','desc')->limit(30);
        }
        $return = $query->get()->toArray();
        return !empty($return) ? response()->json($return,200) :
            response()->json(['message'=>'Nenhum auditoria'],400);
    }
}

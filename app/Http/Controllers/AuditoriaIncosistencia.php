<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use League\Csv\Writer;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;


class AuditoriaIncosistencia extends Controller
{
    //
    public function getAuditoriaDaily($date = null)
    {
        $getAll = DB::table('auditoria_inconsistencias')
            ->where('data_processamento',is_null($date) ? date('Y-m-d',strtotime('-1 day')) : $date)
            ->get();
        if(count($getAll)>0) {
            return response()->json($getAll,200);
        }
        return response()->json(['message'=>'Nada auditado neste dia'],400);
    }

    public function generateRel(Request $request)
    {
        $data = $request->all();
        $objRel = json_decode($data['objRel']);
        //var_dump($objRel); exit;
        $writer = Writer::createFromFileObject(new \SplTempFileObject());
        $writer->setDelimiter(';');
        $writer->setInputEncoding('utf-8');
        $writer->insertOne(['Lead_id','id_tipo','Tipo de Cadastro','Data de processamento da CRON','Descricao do Erro']);
        foreach ($objRel as $item){
            $writer->insertOne([$item->lead_id,$item->id_tipo,$item->tipo,$item->data_processamento,utf8_decode($item->desc_erro)]);
        }
        $name = "relatorio_".time().".csv";
        $file_name = storage_path('logs/relatoriosCsv/'.$name);
        file_put_contents($file_name,$writer);
        //return response()->download($file_name);
        return $name;
    }

    public function downloadRel($fileName)
    {
        $name = $fileName;
        $fileName = storage_path('logs/relatoriosCsv/'.$fileName);
        $headers = [
            'Content-Description'=>'File Transfer',
            'Content-Disposition'=>'attachment; filename='.$name,
            'Content-Type'=>'application/octet-stream',
            'Content-Transfer-Encoding'=>'binary',
            'Content-Length'=>filesize($fileName),
            'Cache-Control'=>'must-revalidate, post-check=0, pre-check=0',
            'Pragma'=> 'public',
            'Expires'=>0
        ];
        return response()->file($fileName,$headers);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function parseXml($data)
    {

    	$output = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:top="http://www.topdown.com.br">
    				<soapenv:Header/>
    				<soapenv:Body>'
    				.$data.
    				'</soapenv:Body>
				</soapenv:Envelope>';
		$output = (string) str_replace('<?xml version="1.0" encoding="UTF-8"?>',"",$output);
        return (string) $output;
    }
}

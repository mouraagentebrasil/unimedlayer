<?php
/**
 * Created by PhpStorm.
 * User: felipemoura
 * Date: 20/10/2016
 * Time: 11:45
 */

namespace App\Http\Controllers;

use App\Carteira;
use App\Http\Controllers\CarteiraController;
use App\Lead;
use App\Plano;
use App\Titular;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\ServiceXml\UnimedGeraPropostaIndividualXmlService as PropostaIndividual;
use GuzzleHttp\Client;
use App\AuditoriaCarteira;

class CarteiraIndividualController extends Controller
{
    public function geraCarteiraUnicaUnimed(Request $request)
    {
        $data = $request->all();
        $user = $request->session()->get('user')[0][0];
        $dateFormat = \DateTime::createFromFormat('d/m/Y',$data['data_inclusao']);
        $data['data_inclusao'] = $dateFormat->format('Y-m-d');
        //var_dump($this->processDataTitulares($data,$user)); exit;
        return response()->json($this->processDataTitulares($data,$user),200);
    }

    public function processDataTitulares($data,$user)
    {
        $getPlanID = function($str_plano){
            return strrpos($str_plano["plan_nome"],"PLUS") == true ? 459 : 460;
        };
        $propostaIndividual = new PropostaIndividual();
        $titular = $data;
        $responsavel = null;
        $lead = new Lead();
        $leadDados = $lead->where('lead_cpf',$data['lead_cpf'])->first();
        $planID = $getPlanID(Plano::getPlanoById($leadDados->plan_id));
        //verifica data de nascimento
        if($propostaIndividual->calculaIdade($data['nascimento']) < 18){
            //precisa de responsavel
            $responsavel = $this->getResponsavelFormated($data['lead_cpf']);
        }
        //Valida dados
        $prevalider = $this->preValider($titular,$user,$titular['data_inclusao']);
        if($prevalider['status']==true){
            return $prevalider['retorno'];
        }
        return $this->postGeraCarteiraUnimed(
            $titular,$responsavel,
            $titular['data_inclusao'],
            $user,$planID);
    }

    public function postGeraCarteiraUnimed($titular,$responsavel,$dataOperacao, $user,$planID)
    {
        $response = [];
        $url = getenv("WEB_SERVER_UNIMED");
        $client = new Client();
        $proposta = new PropostaIndividual();
        $carteira = new CarteiraController();

        $xml = $proposta->processaXmlSoapUnimedGeraPropostaIndividual($titular,$responsavel,$dataOperacao,$planID);
        //log da chamada
        file_put_contents(
            storage_path('logs/xmlGerado/xml_'.$titular['lead_id'].'_ID_'.time().'.txt'),
            $xml."\n\n__________________________________________________\n\n".PHP_EOL,
            FILE_APPEND
        );

        $r = $client->request('POST', $url, [
            'headers'=> [
                'Content-Type' => 'text/xml; charset=UTF8'
            ],
            'body' => $xml
        ]);
        $response = $carteira->processSoapResponse(
                new \GreenCape\Xml\Converter($r->getBody()->getContents()),
                $titular,
                $user,
                $titular['data_inclusao']
        );
        return $response;
    }

    public function getResponsavelFormated($lead_cpf)
    {
        $lead = new Lead();
        $getResposavel = $lead->getInfoLead($lead_cpf,'lead_cpf');
        //var_dump($getResposavel[0]); exit;
        if(empty($getResposavel)) return null;
        $objResponsavel = [
            'nome'=>$getResposavel[0]['lead_nome'],
            'cpf'=>$getResposavel[0]['lead_cpf'],
            'nascimento'=>$getResposavel[0]['lead_nascimento'],
            'civil'=>$getResposavel[0]['lead_civil'] == "solteiro" ? 1 : 2,
            'sexo'=>$getResposavel[0]['lead_sexo'] == "femenino" ? "F" : "M",
            'email'=>$getResposavel[0]['lead_email'],
        ];
        return $objResponsavel;
    }

    public function preValider($titular,$user,$dataOperacao)
    {
        $status = false;
        $proposta = new PropostaIndividual();
        $retorno = [];
        $return = ['retorno'=>null,'status'=>false];

        $retorno = [
            'tipo_cadastro_agente_brasil'=> $titular['tipo']=="titular_pagador" ? "Titular Pagador" : ucfirst($titular['tipo']),
            'tipo_cadastro_unimed'=>'Titular',
            'nome'=>$titular['nome'],
            'lead_cpf'=>$titular['lead_cpf'],
            'carteira'=>null,
            'proposta'=>null,
            'data_inclusao'=>$dataOperacao,
            'status'=>null,
            'message'=>'',
            'origem'=>null
        ];

        if(Lead::where('lead_id',$titular['lead_id'])->whereIn('lead_status',['cancelado','cancelado pagamento','cancelado duplicado'])->first() != null){
            $retorno['message'] .= " | Lead Cancelado | ";
            $status = true;
        }
        //testa se o lead tem mais de um titular na base agente brasil
        if(Titular::where('lead_id',$titular['lead_id'])->count() > 1){
            $retorno['message'] .= " | Lead com mais de um Titular | ";
            $status = true;
        }
        //testa se o lead é menor de idade
        if($proposta->calculaIdade(Lead::getDataNascimentoLead($titular['lead_id'])) < 18){
            $retorno['message'] .= " | Lead ID não pode ser menor de idade | ";
            $status = true;
        }

        if($titular['tipo']=="titular" || $titular['tipo']=="titular_pagador" ){
            //testa se o titular ou beneficiario é maior e se for maior testa se falta informações
            if($proposta->calculaIdade($titular['nascimento']) > 18) {
                if ($titular['cpf'] == "") {
                    $retorno['message'] .= " | CPF em branco | ";
                    $status = true;
                }
            }
        }
        //Essa verificação é redundante, mas pode acontecer
        if($titular['tipo']=="titular_pagador" && $proposta->calculaIdade($titular['nascimento']) < 18){
            $retorno['message'] .= " | Titular pagador não pode ser menor de idade | ";
            $status = true;
        }

        if($titular['logradouro'] == "") {
            $retorno['message'] .= " | Logradouro em branco | ";
            $status = true;
        }

        if($titular['nascimento'] == "") {
            $retorno['message'] .= " | Nascimento em branco | ";
            $status = true;
        }
        if($titular['tipo']=="titular" && $titular['cpf'] == $titular['lead_cpf']){
            $retorno['message'] .= " | Titular com mesmo CPF do LEAD somente pagador | ";
            $status = true;
        }

        if($titular['tipo']=="benificiario" && $titular['cpf'] == $titular['lead_cpf']){
            $retorno['message'] .= " | Beneficiario com mesmo CPF do LEAD | ";
            $status = true;
        }
        //grava auditoria caso o status seja true (ou seja errado)
        if($status==true){
            AuditoriaCarteira::create([
                'come_id'=>$user['come_id'],
                'lead_id'=>$titular['lead_id'],
                'obj_retorno'=>json_encode($retorno),
                'data_operacao'=>date('Y-m-d H:i:s')
            ]);
        }

        $return['retorno'] = $retorno;
        $return['status'] = $status;
        return $return;
    }


}
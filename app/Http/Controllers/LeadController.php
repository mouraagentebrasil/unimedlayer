<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use GuzzleHttp\Client;
use App\Beneficiario;
use App\Titular;
use App\Lead;
use App\ServiceXml\UnimedGeraPropostaXmlService as Proposta;


class LeadController extends Controller
{
    protected $busca_tipo = [
        'proposta'=>'top:NumeroProposta',
        'cpf'=>'top:CpfBeneficiario',
        'nome'=>'top:NomeBeneficiario',
        'card'=>'top:NumeroBeneficiario'
    ];

    public function searchLead(Request $request)
    {
        $data = $request->all();
        if(empty($data['type']) || empty($data['value'])) return response()->json(['message'=>'Favor preencha os campos'],400);
        try {
	    	$url = getenv("WEB_SERVER_UNIMED");
	    	$client = new Client();

	    	$r = $client->request('POST', $url, [
	    		'headers'=> [
	    		 	'Content-Type' => 'text/xml; charset=UTF8'
	    		],
			    'body' => $this->getXmlConsultarPropostaPf($data)
			]);
			$xml = new \GreenCape\Xml\Converter($r->getBody()->getContents());
            $data = $xml->data;
            if($data['soap:Envelope']['soap:Body']['ConsultarPropostaPfResponse']['ConsultarPropostaPfResult'][1]['Contrato'][0]['NumeroContrato']==null){
                $dataHistorico = $data['soap:Envelope']['soap:Body']['ConsultarPropostaPfResponse']['ConsultarPropostaPfResult'][0]['Mensagem'];
                return response()->json($data['soap:Envelope']['soap:Body']['ConsultarPropostaPfResponse']['ConsultarPropostaPfResult'][0]['Mensagem'],400);
            }
            $dataHistorico = $data['soap:Envelope']['soap:Body']['ConsultarPropostaPfResponse']['ConsultarPropostaPfResult'];

            $dataRetorno = [
                'situacao'=>$dataHistorico[1]["Contrato"][1]["Situacao"],
                'beneficiarios'=>$dataHistorico[1]["Contrato"]
            ];
            //var_dump($dataRetorno); exit;
            return response()->json($dataRetorno, 200);
    	} catch(\Exception $e){
    		return response()->json(['message'=>$e->getMessage()],400);
    	}
    }

    public function getXmlConsultarPropostaPf($data)
    {
        $tipos = $this->busca_tipo;
        $merge_array = function() use($tipos,$data){
            $array=array();
            foreach ($tipos as $t){
                $array[$t]="";
                if($t==$tipos[$data['type']]){
                    $array[$t]=$data['value'];
                }
            }
            return $array;
        };
        $xml = [
            'top:ConsultarPropostaPf' =>
                [
                    'top:propostaPf' =>[]
                ]
        ];
        $xml['top:ConsultarPropostaPf']['top:propostaPf'] = array_merge($xml['top:ConsultarPropostaPf']['top:propostaPf'],$merge_array($data['type']));
        $xml = new \GreenCape\Xml\Converter($xml);
        return $this->parseXml((string) $xml);
    }

    public function getInfoLeadOrCpf(Request $request,$value,$opcao)
    {
        $lead = new Lead();
        $data = [];
        $leadInfo = $lead->getInfoLead($value,$opcao);
        if(empty($leadInfo)) return response()->json(['message'=>'Nenhum lead encontrado'],400);
        $beneficiarios = $lead->getBeneficiarios($leadInfo);
        $titulares = $lead->getTitulares($leadInfo);
        $proposta = new Proposta($leadInfo[0],$titulares[0],$beneficiarios[0],date('Y-m-d'));
        $mergeTitulares = $proposta->getTitularesMerged();
        //var_dump($mergeTitulares); exit;
        if(empty($mergeTitulares)) return response()->json(['message'=>'Nenhum lead encontrado'],400);
        $titularesRetorno = $lead->setCardUnimedIfExists(array_reverse($mergeTitulares));
        return response()->json($titularesRetorno,200);
    }
}

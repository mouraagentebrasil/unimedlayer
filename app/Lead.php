<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Beneficiario;
use App\Titular;
use App\Carteira;

class Lead extends Model
{
    //
    protected $guarded = ['lead_id'];
    public $timestamps = false;

    public function getInfoLead($value,$type)
    {
        $leadInfo = array();
        $getPlanID = function($str_plano){
            return strrpos($str_plano["plan_nome"],"PLUS") == true ? 459 : 460;
        };

        $getLead = function($value,$type) use ($getPlanID){
            $lead = $this->where($type,$value)->get()->toArray();
            if(empty($lead)) return null;
            $lead[0]['plan_id'] = $getPlanID(Plano::getPlanoById($lead[0]['plan_id']));
            return $lead[0];
        };
        return [0=>$getLead($value,$type)];
    }
    //Todo Refatorar getInfoLead e getInfoLeads
    public function getInfoLeads($cpfs)
    {
        $leadInfo = array();
        $getPlanID = function($str_plano){
            return strrpos($str_plano["plan_nome"],"PLUS") == true ? 459 : 460;
        };

        $getLead = function($cpf) use ($getPlanID){
            $lead = $this->where('lead_cpf',$cpf)->get()->toArray();
            if(empty($lead)) return null;

            $lead[0]['plan_id'] = $getPlanID(Plano::getPlanoById($lead[0]['plan_id']));
            return $lead[0];
        };
        for($i=0;$i<count($cpfs);$i++) {
            $leadInfo[$i] = $getLead($cpfs[$i]);
        }
        return $leadInfo;
    }

    public function getBeneficiarios($leads)
    {
        $benficiarios = [];

        $getBenefeciarios = function($lead) {
            $beneficiario = Beneficiario::where('lead_id',$lead['lead_id'])->get()->toArray();
            if(empty($beneficiario)) return null;
            return $beneficiario;
        };

        for($i=0;$i<count($leads);$i++){
            $benficiarios[$i] = $getBenefeciarios($leads[$i]);
        }
        return $benficiarios;
    }

    public function getTitulares($leads)
    {
        $titulares = [];
        $getTitulares = function($lead) {
            $titular = Titular::where('lead_id',$lead['lead_id'])->get()->toArray();
            if(empty($titular)) return null;
            return $titular[0];
        };
        for($i=0;$i<count($leads);$i++){
            $titulares[$i] = $getTitulares($leads[$i]);
        }
        return $titulares;
    }

    public static function getDataNascimentoLead($lead_id)
    {
        $lead = Lead::select('lead_nascimento')->where('lead_id',$lead_id)->first();
        return $lead->lead_nascimento;
    }

    public function setCardUnimedIfExists($titulares)
    {
        for($i=0;$i<count($titulares);$i++){
            $tipo = in_array($titulares[$i]['tipo'],["titular","titular_pagador"]) ? "titu_id" : "bene_id";
            $titulares[$i]['status'] = "Não importado";

            $objCardUnimed = Carteira::getObjCardUnimedByTitularOrBeneficiario($titulares[$i]);
            //var_dump($objCardUnimed); exit;
            if(!is_null($objCardUnimed)){
                $titulares[$i]['unimed_id'] = $objCardUnimed['card_unimed'];
                $titulares[$i]['status'] = $objCardUnimed['status'];
            }
        }
        return $titulares;
    }

    public function getInfoBeneficiarioOrTitular()
    {

    }

}

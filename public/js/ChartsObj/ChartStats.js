  var stats_dayData;
  var min_leg;
  var avg_leg;
  var max_leg;

  function ajaxGetDataStatsDay(urlData){
    $('#stats_day-chart').html('<canvas id="stats_day" style="height:250px"></canvas>');

    var settings = {
      "url": urlData,
      "method": "GET",
      "statusCode": {
        403: function() {
          alert("Erro ao carregar os dados");
        },
        500: function() {
          alert("Erro ao conectar no servidor");
        }
      }
    }

    $.ajax(settings).done(function (response) {
      unblockGraph('#send_statsDay', '#stats_day');
      for (var key in response.stats) {
        if (key == 'min') min_leg = response.stats[key];
        if (key == 'avg') avg_leg = response.stats[key];
        if (key == 'max') max_leg = response.stats[key];
      }   
      $('#stats_day-l0').html("Min: "+min_leg);
      $('#stats_day-l1').html("Media: "+avg_leg);
      $('#stats_day-l2').html("Max: "+max_leg);   

      stats_dayData = {
        labels: ["00hs", "01hs", "02hs", "03hs", "04hs", "05hs", "06hs", "07hs", "08hs", "09hs", "10hs", "11hs", "12hs", "13hs", "14hs", "15hs", "16hs", "17hs", "18hs", "19hs", "20hs", "21hs", "22hs", "23hs"],
        datasets: [
        {
          label: "Total de acessos",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: response.stats_in_hours_total
        },
        {
          label: "Acessos únicos",
          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: response.stats_in_hours_unique
        }
        ]
      };
      
      var barChartCanvas = $("#stats_day").get(0).getContext("2d");
      var barChart = new Chart(barChartCanvas);
      var barChartData = stats_dayData;
      barChartData.datasets[1].fillColor = "#00a65a";
      barChartData.datasets[1].strokeColor = "#00a65a";
      barChartData.datasets[1].pointColor = "#00a65a";
      var barChartOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
      };

      barChartOptions.datasetFill = false;
      barChart.Bar(barChartData, barChartOptions);

    });    
  };

  function ajaxGetDataStatsMonth(urlData){
    $('#stats_month-chart').html('<canvas id="stats_month" style="height:250px"></canvas>');
    var settings = {
      "url": urlData,
      "method": "GET",
      "statusCode": {
        403: function() {
          alert("Erro ao carregar os dados");
        },
        500: function() {
          alert("Erro ao conectar no servidor");
        }
      }
    }

    $.ajax(settings).done(function (response) {
      unblockGraph('#send_statsMonth', '#stats_month');
      months_days = '';
      days_desc = '';
      days_qntd = '';


      for (var key in response) {
        months_days += key+',';
        days_qntd += response[key].qtd+',';
        days_desc += response[key].w+',';
      }

      for (var key in response.stats) {
        if (key == 'min') min_leg = response.stats[key];
        if (key == 'avg') avg_leg = response.stats[key];
        if (key == 'max') max_leg = response.stats[key];
      }   
      $('#stats_month-l0').html("Min: "+min_leg);
      $('#stats_month-l1').html("Media: "+avg_leg);
      $('#stats_month-l2').html("Max: "+max_leg);   

      days_array = months_days.split(",");
      qtd_array = days_qntd.split(",");
      dday_array = days_desc.split(",");

      days_array = days_array.splice(0,(days_array.length-2));
      qtd_array = qtd_array.splice(0,(qtd_array.length-2));

      console.log('days_desc', dday_array.splice(0,(dday_array.length-2)));
      console.log('days_array', days_array);
      
      stats_monthData = {
        labels: days_array,
        datasets: [
        {
          label: "Total de acessos",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: qtd_array
        }
        ]
      };
      var barStatsMonthCanvas = $("#stats_month").get(0).getContext("2d");
      var barStatsMonth = new Chart(barStatsMonthCanvas);
      //var barStatsMonthData = stats_dayData;
      stats_monthData.datasets[0].fillColor = "#00a65a";
      stats_monthData.datasets[0].strokeColor = "#00a65a";
      stats_monthData.datasets[0].pointColor = "#00a65a";
      var barStatsMonthOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
      };

      barStatsMonthOptions.datasetFill = false;
      barStatsMonth.Bar(stats_monthData, barStatsMonthOptions);
    });
    
  };

  function ajaxGetDataStatsYear(urlData){
    $('#stats_year-chart').html('<canvas id="stats_year" style="height:250px"></canvas>');
    var settings = {
      "url": urlData,
      "method": "GET",
      "statusCode": {
        403: function() {
          alert("Erro ao carregar os dados");
        },
        500: function() {
          alert("Erro ao conectar no servidor");
        }
      }
    }

    $.ajax(settings).done(function (response) {
      unblockGraph('#send_statsYear', '#stats_year');
      year_month = '';
      month_desc = '';
      month_qntd = '';
      
      for (var key in response.stats) {
        if (key == 'min') min_leg = response.stats[key];
        if (key == 'avg') avg_leg = response.stats[key];
        if (key == 'max') max_leg = response.stats[key];
      }   
      $('#stats_year-l0').html("Min: "+min_leg);
      $('#stats_year-l1').html("Media: "+avg_leg);
      $('#stats_year-l2').html("Max: "+max_leg);   

      for (var key in response) {
        year_month += key+',';
        month_qntd += response[key].qtd+',';
        month_desc += response[key].m+',';
      }
      months_array = year_month.split(",");
      qtd_array = month_qntd.split(",");
      monthd = month_desc.split(",");

      months_array = months_array.splice(0,(months_array.length-2));
      qtd_array = qtd_array.splice(0,(qtd_array.length-2));
      desc_array = monthd.splice(0,(monthd.length-2));

      stats_yearData = {
        labels: desc_array,
        datasets: [
        {
          label: "Total de acessos",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: qtd_array
        }
        ]
      };
      var barStatsYearCanvas = $("#stats_year").get(0).getContext("2d");
      var barStatsYear = new Chart(barStatsYearCanvas);
      //var barStatsMonthData = stats_dayData;
      stats_yearData.datasets[0].fillColor = "#00a65a";
      stats_yearData.datasets[0].strokeColor = "#00a65a";
      stats_yearData.datasets[0].pointColor = "#00a65a";
      var barStatsYearOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
      };

      barStatsYearOptions.datasetFill = false;
      barStatsYear.Bar(stats_yearData, barStatsYearOptions);
    });
    
  };

  function ajaxGetDataStatsDaysRange(urlData){
    $('#stats_daysRange-chart').html('<canvas id="stats_daysRange" style="height:250px"></canvas>');
    var settings = {
      "url": urlData,
      "method": "GET",
      "statusCode": {
        403: function() {
          alert("Erro ao carregar os dados");
        },
        500: function() {
          alert("Erro ao conectar no servidor");
        }
      }
    }

    $.ajax(settings).done(function (response) {
      unblockGraph('#send_statsDaysRange', '#stats_daysRange');
      srd_w = '';
      srd_qtd = '';
      srd_d = '';

      
      for (var key in response.stats) {
        if (key == 'min') min_leg = response.stats[key];
        if (key == 'avg') avg_leg = response.stats[key];
        if (key == 'max') max_leg = response.stats[key];
      }   
      $('#stats_daysRange-l0').html("Min: "+min_leg);
      $('#stats_daysRange-l1').html("Media: "+avg_leg);
      $('#stats_daysRange-l2').html("Max: "+max_leg);   

      for (var key in response) {
        srd_w += response[key].d+'-'+response[key].w+',';
        srd_qtd += response[key].qtd+',';
        srd_d += response[key].d+',';
      }
      srd_w = srd_w.split(",");
      srd_qtd = srd_qtd.split(",");
      srd_d = srd_d.split(",");

      srd_warray = srd_w.splice(0,(srd_w.length-2));
      srd_qtdarray = srd_qtd.splice(0,(srd_qtd.length-2));
      srd_darray = srd_d.splice(0,(srd_d.length-2));

      stats_Data = {
        labels: srd_warray,
        datasets: [
        {
          label: "Total de acessos",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: srd_qtdarray
        }
        ]
      };
      var barStatsCanvas = $("#stats_daysRange").get(0).getContext("2d");
      var barStats = new Chart(barStatsCanvas);
      //var barStatsMonthData = stats_dayData;
      stats_Data.datasets[0].fillColor = "#00a65a";
      stats_Data.datasets[0].strokeColor = "#00a65a";
      stats_Data.datasets[0].pointColor = "#00a65a";
      var barStatsOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
      };

      barStatsOptions.datasetFill = false;
      barStats.Bar(stats_Data, barStatsOptions);
    });
    
  };

  function ajaxGetDataStatsDaysRangeByCampaing(urlData){

    $('#stats_daysRangeByCampaing-chart').html('<canvas id="stats_daysRangeByCampaing" style="height:250px"></canvas>');

    var settings = {
      "url": urlData,
      "method": "GET",
      "statusCode": {
        403: function() {
          alert("Erro ao carregar os dados");
        },
        500: function() {
          alert("Erro ao conectar no servidor");
        }
      }
    }

    $.ajax(settings).done(function (response) {
      unblockGraph('#send_statsDaysRangeByCampaing', '#stats_daysRangeByCampaing');
      srd_w = '';
      srd_qtd = '';
      srd_d = '';

      
      for (var key in response.stats) {
        if (key == 'min') min_leg = response.stats[key];
        if (key == 'avg') avg_leg = response.stats[key];
        if (key == 'max') max_leg = response.stats[key];
      }   
      $('#stats_daysRangeByCampaing-l0').html("Min: "+min_leg);
      $('#stats_daysRangeByCampaing-l1').html("Media: "+avg_leg);
      $('#stats_daysRangeByCampaing-l2').html("Max: "+max_leg);   

      for (var key in response) {
        srd_w += response[key].d+'-'+response[key].w+',';
        srd_qtd += response[key].qtd+',';
        srd_d += response[key].d+',';
      }
      srd_w = srd_w.split(",");
      srd_qtd = srd_qtd.split(",");
      srd_d = srd_d.split(",");

      srd_warray = srd_w.splice(0,(srd_w.length-2));
      srd_qtdarray = srd_qtd.splice(0,(srd_qtd.length-2));
      srd_darray = srd_d.splice(0,(srd_d.length-2));

      stats_Data = {
        labels: srd_warray,
        datasets: [
        {
          label: "Total de acessos",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: srd_qtdarray
        }
        ]
      };
      var barStatsCanvas = $("#stats_daysRangeByCampaing").get(0).getContext("2d");
      var barStats = new Chart(barStatsCanvas);
      //var barStatsMonthData = stats_dayData;
      stats_Data.datasets[0].fillColor = "#00a65a";
      stats_Data.datasets[0].strokeColor = "#00a65a";
      stats_Data.datasets[0].pointColor = "#00a65a";
      var barStatsOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
      };

      barStatsOptions.datasetFill = false;
      barStats.Bar(stats_Data, barStatsOptions);
    });
    
  };
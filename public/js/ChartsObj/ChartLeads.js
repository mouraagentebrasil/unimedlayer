  var stats_dayData;
  var min_leg;
  var avg_leg;
  var max_leg;


  function ajaxGetDataLeadsDaysRangeByCampaing(urlData){
    $('#leads_daysRangeByCampaing-chart').html('<canvas id="leads_daysRangeByCampaing" style="height:250px"></canvas>');
    var settings = {
      "url": urlData,
      "method": "GET",
      "statusCode": {
        403: function() {
          alert("Erro ao carregar os dados");
        },
        500: function() {
          alert("Erro ao conectar no servidor");
        }
      }
    }

    $.ajax(settings).done(function (response) {
      unblockGraph('#send_leadsDaysRangeByCampaing', '#leads_daysRangeByCampaing');
      srd_w = '';
      srd_qtd = '';
      srd_d = '';

      for (var key in response) {
        srd_w += response[key].d+'-'+response[key].w+',';
        srd_qtd += response[key].qtd+',';
        srd_d += response[key].d+',';
      }
      srd_w = srd_w.split(",");
      srd_qtd = srd_qtd.split(",");
      srd_d = srd_d.split(",");

      srd_warray = srd_w.splice(0,(srd_w.length-2));
      srd_qtdarray = srd_qtd.splice(0,(srd_qtd.length-2));
      srd_darray = srd_d.splice(0,(srd_d.length-2));

      stats_Data = {
        labels: srd_warray,
        datasets: [
        {
          label: "Total de leads",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: srd_qtdarray
        }
        ]
      };
      var barStatsCanvas = $("#leads_daysRangeByCampaing").get(0).getContext("2d");
      var barStats = new Chart(barStatsCanvas);
      //var barStatsMonthData = stats_dayData;
      stats_Data.datasets[0].fillColor = "#00a65a";
      stats_Data.datasets[0].strokeColor = "#00a65a";
      stats_Data.datasets[0].pointColor = "#00a65a";
      var barStatsOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
      };

      barStatsOptions.datasetFill = false;
      barStats.Bar(stats_Data, barStatsOptions);
    });
    
  };

  function ajaxGetDataLeadsConvertionsRange(urlData){
    $('#leads_convertionsRange-chart').html('<canvas id="leads_convertionsRange" style="height:250px"></canvas>');
    var settings = {
      "url": urlData,
      "method": "GET",
      "statusCode": {
        403: function() {
          alert("Erro ao carregar os dados");
        },
        500: function() {
          alert("Erro ao conectar no servidor");
        }
      }
    }

    $.ajax(settings).done(function (response) {
      unblockGraph('#send_leadsConvertionsRange', '#leads_convertionsRange');
      srd_w = '';
      srd_qtd = '';
      srd_d = '';

      for (var key in response) {
        srd_w += response[key].d+'-'+response[key].w+',';
        srd_qtd += response[key].qtd+',';
        srd_d += response[key].d+',';
      }
      srd_w = srd_w.split(",");
      srd_qtd = srd_qtd.split(",");
      srd_d = srd_d.split(",");

      srd_warray = srd_w.splice(0,(srd_w.length-2));
      srd_qtdarray = srd_qtd.splice(0,(srd_qtd.length-2));
      srd_darray = srd_d.splice(0,(srd_d.length-2));

      stats_Data = {
        labels: srd_warray,
        datasets: [
        {
          label: "Total de leads",
          fillColor: "rgba(210, 214, 222, 1)",
          strokeColor: "rgba(210, 214, 222, 1)",
          pointColor: "rgba(210, 214, 222, 1)",
          pointStrokeColor: "#c1c7d1",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(220,220,220,1)",
          data: srd_qtdarray
        }
        ]
      };
      var barStatsCanvas = $("#leads_convertionsRange").get(0).getContext("2d");
      var barStats = new Chart(barStatsCanvas);
      //var barStatsMonthData = stats_dayData;
      stats_Data.datasets[0].fillColor = "#00a65a";
      stats_Data.datasets[0].strokeColor = "#00a65a";
      stats_Data.datasets[0].pointColor = "#00a65a";
      var barStatsOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
      };

      barStatsOptions.datasetFill = false;
      barStats.Bar(stats_Data, barStatsOptions);
    });
    
  };
@extends('adminlte::page')

@section('content_header')
    <h1>Operações Por Usuario (CPF ou Lead_ID)</h1>
@stop

@section('content')
    <div class="box">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Preencha os campos</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" class="formBuscaInforByLeadOrCpf" action="{{url('/getInfoLeadOrCpf')}}"
                  data-url-gera-carteira = "{{url('/geraCarteiraUnicaUnimed')}}"
                  data-url-exclui-carteira = "{{ url('/excluiCarteira')}}">
                <div class="box-body">
                    <div class="form-group col-md-3">
                        <label>CPF ou Lead</label>
                        <input type="text" class="form-control col-md-3" id="value" name="value" placeholder="Insira o CPF ou o Lead_id de um usuario">
                    </div>
                    <div class="form-group col-md-3">
                        <label>CPF ou Lead_ID</label>
                        <select class="form-control col-md-3" name="opcao" id="opcao" class="form-control ">
                            <option value="lead_cpf">CPF</option>
                            <option value="lead_id">LEAD_ID</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-success btn-flat buscaUsuario">Buscar</button>
                    </div>
                </div>
                <div class="box-footer">

                    <div class="col-xs-06">
                        <div class="response"></div>
                    </div>
                    <hr>
                    <div class="col-xs-06">
                        <div class="response_two_part"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="montaModal"></div>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/agente_brasil_sys.css')}}">
    <link rel="stylesheet" href="{{asset('js/Datepicker/datepicker.css')}}">
@stop

@section('js')
    <script src="{{ asset('js/Datepicker/datepicker.js') }}"></script>
    <script src="{{ asset('js/blockUI.js') }}"></script>
    <script>
        $(function () {
            $('#datepicker').datepicker({
                "useCurrent": true,
                "setDate": new Date(),
                "autoclose": true,
                "language": 'pt',
                "format": "dd/mm/yyyy"
            });
            //gatilho de busca de LEAD ou CPF
            $('.formBuscaInforByLeadOrCpf').on('submit',function(e){
                e.preventDefault();
                if($('#value').val()!=""){
                    ajaxLoadLead();
                } else {
                    alert('Preencha o CPF ou Lead!');
                }
                $('.response_two_part').empty();
            });

            var ajaxLoadLead = function(){
                var url = $('.formBuscaInforByLeadOrCpf').attr('action')+"/"+$('#value').val()+"/"+$('#opcao').val();
                var promise = $.ajax({method:"get",url: url});
                promise.done(function (resp) {
                    $('.response').html(montaTable(resp));
                    console.log(resp)
                });
                promise.fail(function(resp){
                    alert(resp.responseJSON.message);
                    console.log(resp.responseJSON.message);
                });
            };

            montaTable = function(data){

                html = "<div class='col-md-12'>";
                html +='<table class="table table-hover">' +
                        '<thead>' +
                        '<tr>' +
                        '<th>LEAD CPF: </th>'+
                        '<th>Carteira</th>' +
                        '<th>Nome</th>' +
                        '<th>Status</th>' +
                        '<th>Ação</th>' +
                        '</tr>'+
                        '</thead>'+
                        '<tbody>';
                for(i=0;i<data.length;i++) {

                    var classe = "";
                    if(data[i].status == "null"){
                        classe = "dangerAgenteMaxError";
                    } else if(data[i].status=="error"){
                        classe = "dangerAgenteLogError";
                    }

                    html += '<tr class="' + classe + '">' +
                            '<td>' + data[i].lead_cpf + '</td>' +
                            '<td>' + data[i].unimed_id + '</td>' +
                            '<td>' + data[i].nome + '</td>' +
                            '<td>' + setStatusBadge(data[i].status) + '</td>' +
                            '<td>' + setaAcoes(data[i].status,data[i]) + '</td>' +
                            '</tr>';
                }

                html+= '</tbody>' +
                       '</table>' +
                        '</div>';
                return html;
            };

            setStatusBadge = function(status){
                var html = "";
                var classe = "badge bg-yellow";
                if(status=="Carteira Excluída"){
                    classe="badge bg-red";
                } else if(status=="Carteira Importada"){
                    classe = "badge bg-green";
                }
                return '<span class="'+classe+'">'+status+'</span>';
            };

            setaAcoes = function (status,obj) {
                var html = "<button data-obj-usuario='"+JSON.stringify(obj)+"' class='btn btn-block btn-info btn-flat data-action-user' type='button'>Gerar Carteira</button>";
                if(status == "Carteira Importada"){
                    html = "<button data-obj-usuario='"+JSON.stringify(obj)+"' class='btn btn-block btn-danger btn-flat data-action-user' type='button'>Excluir Carteira</button>";
                } else if(status == "Não importado") {
                    html = "<button data-obj-usuario='"+JSON.stringify(obj)+"' class='btn btn-block btn-info btn-flat data-action-user' type='button'>Gerar Carteira</button>";
                }
                return html;
            };

            //gatilho de abrir modal de geração de carteira ou exclusão de carteiras
            $(document).on('click','.data-action-user',function(e){
                e.preventDefault();
                var data = $(this).data('obj-usuario');
                if($(this).text() == "Gerar Carteira" ){

                    $('.montaModal').html(montaModalGerarCarteira(data));
                    $('#datepicker').datepicker({
                        "useCurrent": true,
                        "setDate": new Date(),
                        "autoclose": true,
                        "language": 'pt',
                        "format": "dd/mm/yyyy"
                    });
                    $('.modal').modal('show');
                } else {
                    $('.montaModal').html(montaModalExcluirCarteira(data));
                    $('#datepicker').datepicker({
                        "useCurrent": true,
                        "setDate": new Date(),
                        "autoclose": true,
                        "language": 'pt',
                        "format": "dd/mm/yyyy"
                    });
                    $('.modal').modal('show');
                }
            });

            montaModalGerarCarteira = function(data){
                var html = "";
                html =
                        '<div class="modal"> ' +
                            '<div class="modal-dialog"> ' +
                                '<div class="modal-content"> ' +
                                    '<div class="modal-header"> ' +
                                        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"> ' +
                                        '<span aria-hidden="true">×</span></button> ' +
                                        '<h4 class="modal-title">Gerar Carteira para: '+data.nome+'</h4> ' +
                                    '</div> ' +
                                    '<div class="modal-body"> ' +
                                        '<div class="row">'+
                                            '<form class="">'+
                                            '<div class="col-md-6">'+
                                                '<label>Data de Inclusao</label>'+
                                                '<div class="input-group date col-md-9">'+
                                                    '<div class="input-group-addon">'+
                                                        '<i class="fa fa-calendar"></i>'+
                                                    '</div>'+
                                                    '<input type="text" name="data-inclusao" id="datepicker" class="form-control col-md-3">'+
                                                '</div>'+
                                            '</div>'+
                                            '</form>'+
                                        '</div>'+
                                    '</div> ' +
                                    '<div class="modal-footer"> ' +
                                            '<button type="button" class="btn btn-default pull-left btn-flat" data-dismiss="modal">Close</button> ' +
                                            "<button data-obj-usuario='"+JSON.stringify(data)+"' type='button' class='btn btn-primary btn-flat gerarUnicaCarteira'>Gerar Carteira</button> " +
                                    '</div> ' +
                                '</div>' +
                                '<!-- /.modal-content --> ' +
                                '</div>' +
                            '<!-- /.modal-dialog -->' +
                            '</div>';
                return html;
            };

            montaTablePartTwo = function (data) {
                html = "<div class='col-md-12'>";
                html +='<table class="table table-hover">' +
                        '<thead>' +
                        '<tr>' +
                        '<th>LEAD CPF: </th>'+
                        '<th>Tipo Cadastro (AgenteBrasil): </th>'+
                        '<th>Tipo Cadastro (Unimed): </th>'+
                        '<th>Carteira</th>' +
                        '<th>Nome</th>' +
                        '<th>Status</th>' +
                        '<th>Retorno do Servidor</th>' +
                        '<th>Proposta</th>' +
                        '<th>Data Operação</th>' +
                        '</tr>'+
                        '</thead>'+
                        '<tbody>';

                        var classe = "";
                        if(data.status == null){
                            classe = "dangerAgenteMaxError";
                        } else if(data.status=="error"){
                            classe = "dangerAgenteLogError";
                        }
                        html +=
                                '<tr class="'+classe+'">'+
                                '<td>'+data.lead_cpf+'</td>'+
                                '<td>'+data.tipo_cadastro_agente_brasil+'</td>'+
                                '<td>'+data.tipo_cadastro_unimed+'</td>'+
                                '<td>'+data.carteira+'</td>'+
                                '<td>'+data.nome+'</td>'+
                                '<td>'+data.status+'</td>'+
                                '<td>'+data.message+'</td>'+
                                '<td>'+data.proposta+'</td>'+
                                '<td>'+data.data_inclusao+'</td>'+
                                '</td>';

                html += "</tbody></table></div>";
                return html;
            };

            //gatilho de geração de carteira
            $(document).on('click','.gerarUnicaCarteira',function (e) {
                dataInclusao = $('input[name=data-inclusao]').val();
                if(dataInclusao!=""){
                    $('.modal').modal('hide');
                    $.blockUI({'message':"Processando..."});
                    obj_usuario = $(this).data('obj-usuario');
                    obj_usuario.data_inclusao = dataInclusao;
                    url = $('.formBuscaInforByLeadOrCpf').data('url-gera-carteira');
                    ajaxGeraCarteiraUnica(obj_usuario,url);
                } else {
                    alert('Preencha a data de Inclusao');
                }
            });

            //ajax geracao de carteira unica
            var ajaxGeraCarteiraUnica = function(obj,url){

                var promise = $.ajax({method:"post",url: url, data: obj});
                promise.done(function(resp){
                    $.unblockUI();
                    $('.modal').remove();
                    $('.response_two_part').html(montaTablePartTwo(resp));
                    //recarrega as informações
                    ajaxLoadLead();
                    console.log(resp);
                });
                promise.fail(function(resp){
                    $.unblockUI();
                    $('.modal').modal('hide');
                    $('.modal').remove();
                    //TODO prevalider
                    alert("Erro");
                    console.log(resp);
                });
            };

            var montaModalExcluirCarteira = function(data){
                var html = "";
                html =
                        '<div class="modal">' +
                            '<div class="modal-dialog">' +
                                '<div class="modal-content"> ' +
                                    '<div class="modal-header"> ' +
                                        '<button type="button" class="close" data-dismiss="modal" aria-label="Close"> ' +
                                        '<span aria-hidden="true">×</span></button> ' +
                                        '<h4 class="modal-title">Excluir a Carteira de: '+data.nome+' (Unimed Card: '+data.unimed_id+')</h4> ' +
                                    '</div>' +
                                '<div class="modal-body"> ' +
                                    '<div class="row">'+
                                        '<form class="">'+
                                            '<div class="col-md-6">'+
                                                '<label>Data de Exclusão</label>'+
                                                '<div class="input-group date col-md-9">'+
                                                    '<div class="input-group-addon">'+
                                                    '<i class="fa fa-calendar"></i>'+
                                                    '</div>'+
                                                    '<input type="text" name="data-exclusao" id="datepicker" class="form-control col-md-3">'+
                                                '</div>'+

                                                '<div class="form-group">'+
                                                    '<label>Observação</label>'+
                                                    '<input type="text" class="form-control" id="" name="observacao_exclusao" maxlength="22" placeholder="Frase até 22 caracteres">'+
                                                '</div>'+
                                                '<div class="form-group">'+
                                                    '<label>Motivo</label>'+
                                                    '<select class="form-control" name="motivo_exclusao" id="motivo_exclusao" class="form-control ">'+
                                                    '<option value="46">Inclusão indevida</option>'+
                                                    '<option value="93">Inadimplência</option>'+
                                                    '<option value="10">A pedido do beneficiário</option>'+
                                                    '<option value="81">Troca Titularidade</option>'+
                                                    '<option value="5">Falecimento</option>'+
                                                    '<option value="8">Transferência Carteira</option>'+
                                                    '<option value="31">Aposentadoria</option>'+
                                                    '</select>'+
                                                '</div>'+
                                                '<div class="form-group">'+
                                                    '<label>Motivo</label>'+
                                                    '<select id="origem_exclusao" name="origem_exclusao" class="form-control ">'+
                                                    '<option value="sac">SAC</option>'+
                                                    '<option value="vendas">Vendas</option>'+
                                                    '<option value="financeiro">Financeiro</option>'+
                                                    '<option value="supervisor">Supervisor</option>'+
                                                    '<option value="diretoria">Diretoria</option>'+
                                                    '<option value="outros">Outros</option>'+
                                                    '</select>'+
                                                '</div>'+
                                                '</div>'+
                                            '</div> ' +
                                            '<div class="modal-footer"> ' +
                                            '<button type="button" class="btn btn-default pull-left btn-flat" data-dismiss="modal">Close</button> ' +
                                            "<button data-obj-usuario='"+JSON.stringify(data)+"' type='button' class='btn btn-danger btn-flat excluirCarteira'>" +
                                                "Excluir Carteiras" +
                                            "</button> " +
                                        '</form>'+
                                    '</div> ' +
                                '</div>' +
                            '<!-- /.modal-content --> ' +
                            '</div>' +
                            '<!-- /.modal-dialog -->' +
                        '</div>';
                return html;
            };

            validForm = function () {
                if(
                        $('input[name=data_exclusao]').val() == "" ||
                        $('input[name=observacao_exclusao]').val() == "" ||
                        $('#motivo_exclusao').val() == "" ||
                        $('#origem_exclusao').val() == ""
                ) {
                    return true;
                }
                return false;
            };

            $(document).on('click','.excluirCarteira',function(e){
                e.preventDefault();
                if(!validForm()) {
                    $('.modal').modal('hide');
                    var dataObj = $(this).data('obj-usuario');
                    $.blockUI({message:"Processando..."});
                    var promise = $.ajax({
                        url: $('.formBuscaInforByLeadOrCpf').data('url-exclui-carteira'),
                        method: 'post',
                        data: {
                            'card_unimed': dataObj.unimed_id,
                            'data_exclusao': $('#datepicker').val(),
                            'observacao_exclusao': $('input[name=observacao_exclusao]').val(),
                            'motivo_exclusao': $('#motivo_exclusao').val(),
                            'origem_exclusao': $('#origem_exclusao').val()
                        }
                    });
                    promise.done(function (resp) {
                        $.unblockUI();
                        $('.modal').modal('hide');
                        $('.modal').remove();
                        ajaxLoadLead();
                        $('.response_two_part').html(montaTablePartTwo(resp));
                    });

                    promise.fail(function (resp) {
                        $.unblockUI();
                        $('.modal').modal('hide');
                        $('.modal').remove();
                        alert('Erro, verifique se essa Carteira existe');
                        console.log(resp);
                    });
                } else {
                    alert('Preencha os campos');
                }
            });
        });
    </script>
@stop
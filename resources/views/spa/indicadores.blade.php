@extends('adminlte::page')

@section('content_header')
<h1>Indicadores</h1>
@stop

@section('content')
<section class="content">
  <!-- Statsgrafico-diario -->
  <div class="grafico-diaro">
    <div class="row">
      <div class="col-md-5">
        <!-- Produto -->
        <div class="form-group">
          <label>Produto</label>
          <select id="produto_hora" class="form-control">
            <option value="-1">Selecione o produto</option>
            <option value="unimed">Unimed</option>
            <option value="pet">Plano Pet</option>
            <option value="metlife">Metlife</option>
          </select>
        </div>
      </div>
      <div class="col-md-4">
        <label>Data Inicial</label>
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" name="data_statsDay" id="data_statsDay" class="form-control pull-right" >
        </div>
      </div>
      <div class="col-md-3">
        <label></label>
        <div class="form-group">
          <button type="button" id="send_statsDay" class="btn btn-block btn-primary">Atualizar gráfico diário</button>
        </div>
      </div>
    </div>
    <br />
    <div class="row">
      <div class="col-md-12">
        <!-- Diario -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Acessos por hora</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="chart" id="stats_day-chart">
              <canvas id="stats_day" style="height:250px"></canvas>
            </div>
            <div id="stats_day-legend">
              <ul>
                <li id="stats_day-l0"></li>
                <li id="stats_day-l1"></li>
                <li id="stats_day-l2"></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Statsgrafico-mensal -->
  <div class="grafico-mensal">
    <div class="row">
      <div class="col-md-5">
        <!-- Produto -->
        <div class="form-group">
          <label>Produto</label>
          <select id="produto_dia" class="form-control">
            <option value="-1">Selecione o produto</option>
            <option value="unimed">Unimed</option>
            <option value="pet">Plano Pet</option>
            <option value="metlife">Metlife</option>
          </select>
        </div>
      </div>
      <div class="col-md-4">
        <label>Data Inicial</label>
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" name="data_statsMonth" id="data_statsMonth" class="form-control pull-right" >
        </div>
      </div>
      <div class="col-md-3">
        <label></label>
        <div class="form-group">
          <button type="button" id="send_statsMonth" class="btn btn-block btn-primary">Atualizar gráfico mensal</button>
        </div>
      </div>
    </div>
    <br />
    <div class="row">
      <div class="col-md-12">
        <!-- Diario -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Acessos por dia</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="chart" id="stats_month-chart">
              <canvas id="stats_month" style="height:250px"></canvas>
            </div>
            <div id="stats_month-legend">
              <ul>
                <li id="stats_month-l0"></li>
                <li id="stats_month-l1"></li>
                <li id="stats_month-l2"></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Statsgrafico-anual -->
  <div class="grafico-anual">
    <div class="row">
      <div class="col-md-5">
        <!-- Produto -->
        <div class="form-group">
          <label>Produto</label>
          <select id="produto_mes" class="form-control">
            <option value="-1">Selecione o produto</option>
            <option value="unimed">Unimed</option>
            <option value="pet">Plano Pet</option>
            <option value="metlife">Metlife</option>
          </select>
        </div>
      </div>
      <div class="col-md-4">
        <label>Data Inicial</label>
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" name="data_statsYear" id="data_statsYear" class="form-control pull-right" >
        </div>
      </div>
      <div class="col-md-3">
        <label></label>
        <div class="form-group">
          <button type="button" id="send_statsYear" class="btn btn-block btn-primary">Atualizar gráfico anual</button>
        </div>
      </div>
    </div>
    <br />
    <div class="row">
      <div class="col-md-12">
        <!-- Diario -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Acessos por mês</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="chart" id="stats_year-chart">
              <canvas id="stats_year" style="height:250px"></canvas>
            </div>
            <div id="stats_month-legend">
              <ul>
                <li id="stats_year-l0"></li>
                <li id="stats_year-l1"></li>
                <li id="stats_year-l2"></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- StatsDaysRange -->
  <div class="grafico-statsDaysRange">
    <div class="row">
      <div class="col-md-5">
        <!-- Produto -->
        <div class="form-group">
          <label>Produto</label>
          <select id="produto_statsDaysRange" class="form-control">
            <option value="-1">Selecione o produto</option>
            <option value="unimed">Unimed</option>
            <option value="pet">Plano Pet</option>
            <option value="metlife">Metlife</option>
          </select>
        </div>
      </div>
      <div class="col-md-4">
        <label>Data Inicial</label>
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" name="data_statsDaysRangeIni" id="data_statsDaysRangeIni" class="form-control pull-right" >
        </div>
      </div>
      <div class="col-md-4">
        <label>Data Final</label>
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" name="data_statsDaysRangeFim" id="data_statsDaysRangeFim" class="form-control pull-right" >
        </div>
      </div>
      <div class="col-md-3">
        <label></label>
        <div class="form-group">
          <button type="button" id="send_statsDaysRange" class="btn btn-block btn-primary">Atualizar gráfico diario</button>
        </div>
      </div>
    </div>
    <br />
    <div class="row">
      <div class="col-md-12">
        <!-- Diario -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Acessos diarios</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="chart" id="stats_daysRange-chart">
              <canvas id="stats_daysRange" style="height:250px"></canvas>
            </div>
            <div id="stats_daysRange-legend">
              <ul>
                <li id="stats_daysRange-l0"></li>
                <li id="stats_daysRange-l1"></li>
                <li id="stats_daysRange-l2"></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- getStatsDaysRangeByCampaing -->
  <div class="grafico-statsDaysRangeByCampaing">
    <div class="row">
      <div class="col-md-6">
        <!-- Produto -->
        <div class="form-group">
          <label>Produto</label>
          <select id="produto_statsDaysRangeByCampaing" class="form-control">
            <option value="-1">Selecione o produto</option>
            <option value="unimed">Unimed</option>
            <option value="pet">Plano Pet</option>
            <option value="metlife">Metlife</option>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <!-- Produto -->
        <div class="form-group">
          <label>Campanha</label>
          <select id="campanha_statsDaysRangeByCampaing" class="form-control">
          </select>
        </div>
      </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <label>Data Inicial</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" name="data_statsDaysRangeByCampaingIni" id="data_statsDaysRangeByCampaingIni" class="form-control pull-right" >
          </div>
        </div>
        <div class="col-md-4">
          <label>Data Final</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" name="date_statsDaysRangeByCampaingFim" id="date_statsDaysRangeByCampaingFim" class="form-control pull-right" >
          </div>
        </div>
        <div class="col-md-4">
          <label></label>
          <div class="form-group">
            <button type="button" id="send_statsDaysRangeByCampaing" class="btn btn-block btn-primary">Atualizar gráfico acessos por campanha</button>
          </div>
        </div>
    </div>
    <br />
    <div class="row">
      <div class="col-md-12">
        <!-- Diario -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Acessos diarios por campanha</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="chart" id="stats_daysRangeByCampaing-chart">
              <canvas id="stats_daysRangeByCampaing" style="height:250px"></canvas>
            </div>
            <div id="stats_daysRangeByCampaing-legend">
              <ul>
                <li id="stats_daysRangeByCampaing-l0"></li>
                <li id="stats_daysRangeByCampaing-l1"></li>
                <li id="stats_daysRangeByCampaing-l2"></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- getLeadsDaysRangeByCampaing -->
  <div class="grafico-leadsDaysRangeByCampaing">
    <div class="row">
      <div class="col-md-6">
        <!-- Produto -->
        <div class="form-group">
          <label>Produto</label>
          <select id="produto_leadsDaysRangeByCampaing" class="form-control">
            <option value="-1">Selecione o produto</option>
            <option value="unimed">Unimed</option>
            <option value="pet">Plano Pet</option>
            <option value="metlife">Metlife</option>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <!-- Produto -->
        <div class="form-group">
          <label>Campanha</label>
          <select id="campanha_leadsDaysRangeByCampaing" class="form-control">
          </select>
        </div>
      </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <label>Data Inicial</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" name="data_leadsDaysRangeByCampaingIni" id="data_leadsDaysRangeByCampaingIni" class="form-control pull-right" >
          </div>
        </div>
        <div class="col-md-4">
          <label>Data Final</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" name="date_leadsDaysRangeByCampaingFim" id="date_leadsDaysRangeByCampaingFim" class="form-control pull-right" >
          </div>
        </div>
        <div class="col-md-4">
          <label></label>
          <div class="form-group">
            <button type="button" id="send_leadsDaysRangeByCampaing" class="btn btn-block btn-primary">Atualizar gráfico leads diários</button>
          </div>
        </div>
    </div>
    <br />
    <div class="row">
      <div class="col-md-12">
        <!-- leads  -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Leads diarios</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="chart" id="leads_daysRangeByCampaing-chart">
              <canvas id="leads_daysRangeByCampaing" style="height:250px"></canvas>
            </div>
            <div id="leads_daysRangeByCampaing-legend">
              <ul>
                <li id="leads_daysRangeByCampaing-l0"></li>
                <li id="leads_daysRangeByCampaing-l1"></li>
                <li id="leads_daysRangeByCampaing-l2"></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <!-- getLeadsConvertionsRange -->
  <div class="grafico-leadsConvertionsRange">
    <div class="row">
      <div class="col-md-6">
        <!-- Produto -->
        <div class="form-group">
          <label>Produto</label>
          <select id="produto_leadsConvertionsRange" class="form-control">
            <option value="-1">Selecione o produto</option>
            <option value="unimed">Unimed</option>
            <option value="pet">Plano Pet</option>
            <option value="metlife">Metlife</option>
          </select>
        </div>
      </div>
      <div class="col-md-6">
        <!-- Produto -->
        <div class="form-group">
          <label>Campanha</label>
          <select id="campanha_leadsConvertionsRange" class="form-control">
          </select>
        </div>
      </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <label>Data Inicial</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" name="data_leadsConvertionsRangeIni" id="data_leadsConvertionsRangeIni" class="form-control pull-right" >
          </div>
        </div>
        <div class="col-md-4">
          <label>Data Final</label>
          <div class="input-group date">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" name="date_leadsConvertionsRangeFim" id="date_leadsConvertionsRangeFim" class="form-control pull-right" >
          </div>
        </div>
        <div class="col-md-4">
          <label></label>
          <div class="form-group">
            <button type="button" id="send_leadsConvertionsRange" class="btn btn-block btn-primary">Atualizar gráfico leads convertidos</button>
          </div>
        </div>
    </div>
    <br />
    <div class="row">
      <div class="col-md-12">
        <!-- leads  -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Leads convertidos</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body">
            <div class="chart" id="leads_convertionsRange-chart">
              <canvas id="leads_convertionsRange" style="height:250px"></canvas>
            </div>
            <div id="leads_convertionsRange-legend">
              <ul>
                <li id="leads_convertionsRange-l0"></li>
                <li id="leads_convertionsRange-l1"></li>
                <li id="leads_convertionsRange-l2"></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@stop

@section('css')
<link rel="stylesheet" href="{{asset('js/Datepicker/datepicker.css')}}">
<link rel="stylesheet" href="{{asset('css/agente_brasil_sys.css')}}">
@stop

@section('js')
<script src="{{ asset('js/Datepicker/datepicker.js') }}"></script>
<script src="{{ asset('js/Chartjs/Chart.min.js') }}"></script>
<script src="{{ asset('js/ChartsObj/ChartStats.js') }}"></script>
<script src="{{ asset('js/ChartsObj/ChartLeads.js') }}"></script>
<!-- <script type="text/javascript" src="http://canvasjs.com/assets/script/canvasjs.min.js"></script> -->
<script src="{{ asset('js/blockUI.js') }}"></script>
<script>
$('#produto_statsDaysRangeByCampaing').on('change', function(){
  getCampaingByProduct('#produto_statsDaysRangeByCampaing', '#campanha_statsDaysRangeByCampaing');
})
$('#produto_leadsDaysRangeByCampaing').on('change', function(){
  getCampaingByProduct('#produto_leadsDaysRangeByCampaing', '#campanha_leadsDaysRangeByCampaing');
})
$('#produto_leadsConvertionsRange').on('change', function(){
  getCampaingByProduct('#produto_leadsConvertionsRange', '#campanha_leadsConvertionsRange');
}) 

function getCampaingByProduct(product, campaingInput) {
  $.getJSON("{{ url('/getStatsCampaing')}}" + "/" + $(product).val(), function (data){
       if (data){   
        var option = '<option>Selecione a campanha</option>';
        $.each(data, function(i, obj){
          option += '<option value="'+obj.name+'">'+obj.name+'</option>';
        })
        $(campaingInput).html(option).show(); 
       }else{
        
       }
    })
}

function blockGraph(buttonSend, graphName) {
  $(buttonSend).attr('disabled', true);
}
function unblockGraph(buttonSend, graphName) {
  $(buttonSend).attr('disabled', false);
}

$(function(){
  $('#data_statsDay').datepicker({
    "useCurrent": true,
    "setDate": new Date(),
    "autoclose": true,
    "language": 'pt',
    "format": "yyyy-mm-dd"
  });

  $('#send_statsDay').on('click',function(e){
    var produto_hora = $('#produto_hora').val();
    var date_hora = $('#data_statsDay').val();
    blockGraph('#send_statsDay', '#stats_day');
    ajaxGetDataStatsDay("{{ url('/getGraphsStatsDay')}}" + "/" + produto_hora + "/" + date_hora);             
  });
});

$(function(){
  $('#data_statsMonth').datepicker({
    "startView": "months", 
    "minViewMode": "months",
    "autoclose": true,
    "language": 'pt',
    "format": "yyyy-mm"
  });

  $('#send_statsMonth').on('click',function(e){
    var produto_dia = $('#produto_dia').val();
    var date_dia = $('#data_statsMonth').val();
    blockGraph('#send_statsMonth', '#stats_month');
    ajaxGetDataStatsMonth("{{ url('/getStatsPerMonths')}}" + "/" + produto_dia + "/" + date_dia)              
  });
});

$(function(){
  $('#data_statsYear').datepicker({
    "startView": "years", 
    "minViewMode": "years",
    "autoclose": true,
    "language": 'pt',
    "format": "yyyy"
  });

  $('#send_statsYear').on('click',function(e){
    var produto_mes = $('#produto_mes').val();
    var date_ano = $('#data_statsYear').val();
    blockGraph('#send_statsYear', '#stats_year');
    ajaxGetDataStatsYear("{{ url('/getStatsPerYear')}}" + "/" + produto_mes + "/" + date_ano);             
  });
});

//statsDaysRange
$(function(){
  $('#data_statsDaysRangeIni').datepicker({
    "useCurrent": true,
    "setDate": new Date(),
    "autoclose": true,
    "language": 'pt',
    "format": "yyyy-mm-dd"
  });

  $('#data_statsDaysRangeFim').datepicker({
    "useCurrent": true,
    "setDate": new Date(),
    "autoclose": true,
    "language": 'pt',
    "format": "yyyy-mm-dd"
  });

  $('#send_statsDaysRange').on('click',function(e){
    var produto_statsDaysRange = $('#produto_statsDaysRange').val();
    var date_statsDaysRangeIni = $('#data_statsDaysRangeIni').val();
    var date_statsDaysRangeFim = $('#data_statsDaysRangeFim').val();
    blockGraph('#send_statsDaysRange', '#stats_daysRange');
    ajaxGetDataStatsDaysRange("{{ url('/getStatsDaysRange')}}" + "/" + produto_statsDaysRange + "/" + date_statsDaysRangeIni + "/" + date_statsDaysRangeFim);
  });
});

//statsDaysRangeByCampaing
$(function(){
  $('#data_statsDaysRangeByCampaingIni').datepicker({
    "useCurrent": true,
    "setDate": new Date(),
    "autoclose": true,
    "language": 'pt',
    "format": "yyyy-mm-dd"
  });

  $('#date_statsDaysRangeByCampaingFim').datepicker({
    "useCurrent": true,
    "setDate": new Date(),
    "autoclose": true,
    "language": 'pt',
    "format": "yyyy-mm-dd"
  });

  $('#send_statsDaysRangeByCampaing').on('click',function(e){
    var produto_statsDaysRangeByCampaing = $('#produto_statsDaysRangeByCampaing').val();
    var campanha_statsDaysRangeByCampaing = $('#campanha_statsDaysRangeByCampaing').val();
    var date_statsDaysRangeByCampaingIni = $('#data_statsDaysRangeByCampaingIni').val();
    var date_statsDaysRangeByCampaingFim = $('#date_statsDaysRangeByCampaingFim').val();
    blockGraph('#send_statsDaysRangeByCampaing', '#stats_daysRangeByCampaing');
    ajaxGetDataStatsDaysRangeByCampaing("{{ url('/getStatsDaysRangeByCampaing')}}" + "/" + produto_statsDaysRangeByCampaing + "/" + campanha_statsDaysRangeByCampaing + "/" + date_statsDaysRangeByCampaingIni + "/" + date_statsDaysRangeByCampaingFim);             
  });
});

//leadsDaysRangeByCampaing
$(function(){
  $('#data_leadsDaysRangeByCampaingIni').datepicker({
    "useCurrent": true,
    "setDate": new Date(),
    "autoclose": true,
    "language": 'pt',
    "format": "yyyy-mm-dd"
  });

  $('#date_leadsDaysRangeByCampaingFim').datepicker({
    "useCurrent": true,
    "setDate": new Date(),
    "autoclose": true,
    "language": 'pt',
    "format": "yyyy-mm-dd"
  });

  $('#send_leadsDaysRangeByCampaing').on('click',function(e){
    var produto_leadsDaysRangeByCampaing = $('#produto_leadsDaysRangeByCampaing').val();
    var campanha_leadsDaysRangeByCampaing = $('#campanha_leadsDaysRangeByCampaing').val();
    var date_leadsDaysRangeByCampaingIni = $('#data_leadsDaysRangeByCampaingIni').val();
    var date_leadsDaysRangeByCampaingFim = $('#date_leadsDaysRangeByCampaingFim').val();
    blockGraph('#send_leadsDaysRangeByCampaing', '#leads_daysRangeByCampaing');

    ajaxGetDataLeadsDaysRangeByCampaing("{{ url('/getLeadsDailyRange')}}" + "/" + produto_leadsDaysRangeByCampaing + "/" + campanha_leadsDaysRangeByCampaing + "/" + date_leadsDaysRangeByCampaingIni + "/" + date_leadsDaysRangeByCampaingFim);
  });
});

//leadsConvertionsRange
$(function(){
  $('#data_leadsConvertionsRangeIni').datepicker({
    "useCurrent": true,
    "setDate": new Date(),
    "autoclose": true,
    "language": 'pt',
    "format": "yyyy-mm-dd"
  });

  $('#date_leadsConvertionsRangeFim').datepicker({
    "useCurrent": true,
    "setDate": new Date(),
    "autoclose": true,
    "language": 'pt',
    "format": "yyyy-mm-dd"
  });

  $('#send_leadsConvertionsRange').on('click',function(e){
    var produto_leadsConvertionsRange = $('#produto_leadsConvertionsRange').val();
    var campanha_leadsConvertionsRange = $('#campanha_leadsConvertionsRange').val();
    var data_leadsConvertionsRangeIni = $('#data_leadsConvertionsRangeIni').val();
    var date_leadsConvertionsRangeFim = $('#date_leadsConvertionsRangeFim').val();
    blockGraph('#send_leadsConvertionsRange', '#leads_convertionsRange');

    ajaxGetDataLeadsConvertionsRange("{{ url('/getLeadsConvertions')}}" + "/" + produto_leadsConvertionsRange + "/" + campanha_leadsConvertionsRange + "/" + data_leadsConvertionsRangeIni + "/" + date_leadsConvertionsRangeFim);             
  });
});

</script>
@stop
@extends('adminlte::page')

@section('content_header')
    <h1>Auditoria Carteiras (geradas por este usuario)</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title info">Exibindo as ultimas 30 transações</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-header with-border">
            <form class="buscaAuditorias" action="{{ url('/findAuditoriasPorUsuario')}}">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Data Inicial</label>
                            <div class="input-group date col-md-6">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="data_inicial" class="form-control pull-right" id="datepicker_data_inicial" value="{{date('Y-m-d')}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Data Final</label>
                            <div class="input-group date col-md-6">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="data_final" class="form-control pull-right" id="datepicker_data_final" value="">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button type="button" class="buscaAuditoriasBtn btn btn-success btn-flat">Buscar Auditorias</button>
                </div>
            </form>
            <div class="col-xs-06">
                <div class="response1">
                    <table class="table table-hover dataTable">
                        <thead> 
                        <tr> 
                            <th>LEAD CPF: </th>
                            <th>Tipo Cadastro (AgenteBrasil): </th>
                            <th>Tipo Cadastro (Unimed): </th>
                            <th>Carteira</th> 
                            <th>Nome</th> 
                            <th>Status</th> 
                            <th>Retorno do Servidor Unimed</th> 
                            <th>Proposta</th> 
                            <th>Data de Operação</th> 
                            <th>Data de Inclusão/Exclusão</th>
                            </tr>
                        </thead>
                        <tbody class="response">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/agente_brasil_sys.css')}}">
    <link rel="stylesheet" href="{{asset('js/Datepicker/datepicker.css')}}">
@stop

@section('js')
    <script src="{{ asset('js/Datepicker/datepicker.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/locale/pt-br.js"></script>--}}
    <script>
        $(function () {
            $('#datepicker_data_final').datepicker({
                "useCurrent": true,
                "setDate": new Date(),
                "autoclose": true,
                "language": 'pt',
                "format": "yyyy-mm-dd"
            });
            $('#datepicker_data_inicial').datepicker({
                "useCurrent": true,
                "setDate": new Date(),
                "autoclose": true,
                "language": 'pt',
                "format": "yyyy-mm-dd"
            });
            //inicia a listagem com as ultimas 30 auditorias
            var promiseInitListagem = $.ajax({method:"get",url:$('.buscaAuditorias').attr('action')});
            promiseInitListagem.done(function(resp){
                $('.response').html(montaTable(resp));
                $('.dataTable').DataTable({processing : true});
            });
            promiseInitListagem.fail(function(resp){
                $('.response').html(responseJSON.message);
            });
            $('.buscaAuditoriasBtn').on('click',function(e){
                var dateInicial = $('#datepicker_data_inicial').val();
                var dateFinal = $('#datepicker_data_final').val();
                var url = $('.buscaAuditorias').attr('action')+"/"+$('#datepicker_data_inicial').val();
                url = dateFinal != "" ? url+"/"+dateFinal : url;
                var promiseListagem = $.ajax({method:"get",url:url});
                promiseListagem.done(function(resp){
                   $('.response').html(montaTable(resp));
                    $('.dataTable').DataTable();
                });
                promiseListagem.fail(function(resp){
                    console.log(resp)
                   //$('.response').html(responseJSON.message);
                });
            });
            montaTable = function(data){
                html = "";
                for(i=0;i<data.length;i++){
                    var infoAuditoria = JSON.parse(data[i].obj_retorno);
                    var classe = "";
                    if(infoAuditoria.status == null){
                        classe = "dangerAgenteMaxError";
                    } else if(infoAuditoria.status=="error"){
                        classe = "dangerAgenteLogError";
                    }
                    html +=
                            '<tr class="'+classe+'">'+
                            '<td>'+infoAuditoria.lead_cpf+'</td>'+
                            '<td>'+infoAuditoria.tipo_cadastro_agente_brasil+'</td>'+
                            '<td>'+infoAuditoria.tipo_cadastro_unimed+'</td>'+
                            '<td>'+infoAuditoria.carteira+'</td>'+
                            '<td>'+infoAuditoria.nome+'</td>'+
                            '<td>'+infoAuditoria.status+'</td>'+
                            '<td>'+infoAuditoria.message+'</td>'+
                            '<td>'+infoAuditoria.proposta+'</td>'+
                            '<td>'+infoAuditoria.data_inclusao+'</td>'+
                            '<td>'+data[i].data_operacao+'</td>'+
                            '</td>';
                }
                return html;
            };

        });
    </script>
@stop
@extends('adminlte::page')

@section('content_header')
    <h1>Gera Carteirinhas por CPF</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Insira os cpf separados por ; </h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-header with-border">
            <form class="geraCarteirasForm" action="{{ url('/geraCarteiras')}}">
                <div class="form-group">
                    <div class="input-group date col-md-3">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" name="data" class="form-control pull-right" id="datepicker" value="{{date('d/m/Y')}}">
                    </div>
                </div>
                <div class="form-group lista_cpf">
                    <textarea name="lista_cpf" class="form-control inputListCpf" id="" cols="120" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <span class="total_cpfs"></span> Inseridos: <span class="total_inseridos">0</span>
                </div>
                <div class="form-group">
                    <button type="button" class="geraCarteiras btn btn-success btn-flat">Gerar</button>
                    {{--<button class="btn btn-primary buscar">Gerar</button>--}}
                </div>
            </form>
            <div class="col-xs-06">
                <div class="response"></div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('js/Datepicker/datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('css/agente_brasil_sys.css')}}">
@stop

@section('js')
    <script src="{{ asset('js/Datepicker/datepicker.js') }}"></script>
    <script src="{{ asset('js/blockUI.js') }}"></script>
    <script>
        $(function () {
            $('#datepicker').datepicker({
                "useCurrent": true,
                "setDate": new Date(),
                "autoclose": true,
                "language": 'pt',
                "format": "dd/mm/yyyy"
            });
            var separaCpf = function(){
                var cpfs = {};
                cpfs = $('.inputListCpf').val().split(';');
                cpfs.forEach(function(item,index){
                    if(item ==="") cpfs.splice(index,1);
                });
                return cpfs;
            };

            $('.inputListCpf').on('keyup',function(e){
                num_cpfs = separaCpf();
                console.log(num_cpfs);
                $('.total_inseridos').html(separaCpf().length);
            });

            $('.geraCarteiras').on('click',function (e) {
                //validação simples
                if($('.lista_cpf').val().length > 0 || $('input[name=data]').val().length > 0){
                    $.blockUI({message:"Processando..."});
                    ajaxGeraCarteira();
                } else {
                    alert('Preenca os campos!')
                }
            });

            var ajaxGeraCarteira = function(){
                var promise = $.ajax({method:"post",url: $('.geraCarteirasForm').attr('action'),data:{"cpfs":separaCpf(),dataCriacao:$('input[name=data]').val()}});
                promise.done(function(resp){
                    $.unblockUI();
                    $('.response').html(montaTable(resp));
                });
                promise.fail(function(resp){
                    $.unblockUI();
                    alert(resp.responseJSON.message);
                    console.log(resp.responseJSON.message);
                });
            };

            montaTable = function (data) {
                html = "<div class='col-md-12'>";
                html +='<table class="table table-hover">' +
                        '<thead>' +
                            '<tr>' +
                                '<th>#</th>'+
                                '<th>LEAD CPF: </th>'+
                                '<th>Tipo Cadastro (AgenteBrasil): </th>'+
                                '<th>Tipo Cadastro (Unimed): </th>'+
                                '<th>Carteira</th>' +
                                '<th>Nome</th>' +
                                '<th>Status</th>' +
                                '<th>Retorno do Servidor</th>' +
                                '<th>Proposta</th>' +
                                '<th>Data de Inclusão</th>' +
                            '</tr>'+
                        '</thead>'+
                        '<tbody>';
                for(i=0;i<data.length;i++){
                    for(k=0;k<data[i].length;k++){
                        var classe = "";
                        if(data[i][k].status == null){
                            classe = "dangerAgenteMaxError";
                        } else if(data[i][k].status=="error"){
                            classe = "dangerAgenteLogError";
                        }
                        html +=
                            '<tr class="'+classe+'">'+
                                '<td>'+(k+1)+'</td>'+
                                '<td>'+data[i][k].lead_cpf+'</td>'+
                                '<td>'+data[i][k].tipo_cadastro_agente_brasil+'</td>'+
                                '<td>'+data[i][k].tipo_cadastro_unimed+'</td>'+
                                '<td>'+data[i][k].carteira+'</td>'+
                                '<td>'+data[i][k].nome+'</td>'+
                                '<td>'+data[i][k].status+'</td>'+
                                '<td>'+data[i][k].message+'</td>'+
                                '<td>'+data[i][k].proposta+'</td>'+
                                '<td>'+data[i][k].data_inclusao+'</td>'+
                            '</td>';
                    }
                }
                html += "</tbody></table></div>";
                return html;
            };
//            var validaCpf = function(cpfs){
//              cpfs.forEach(function (item) {
//                  console.log("item: "+item);
//                  console.log("item numero de caracteres: "+item.length);
//                 if(item.length >= 12 && item.length > 0 && item < 10){
//                     $('.lista_cpf').addClass('has-error');
//                 } else {
//                     $('.lista_cpf').removeClass('has-error');
//                 }
//              });
//            };
        });
    </script>
@stop
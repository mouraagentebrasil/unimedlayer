@extends('adminlte::page')

@section('content_header')
    <h1>Buscas</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <form class="findUnimed" action="{{ url('/searchLead')}}">
                <div class="form-group">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-3">
                                <input type="text" class="form-control col-xs-4" name="value" id="value" placeholder="">
                            </div>
                            <div class="col-xs-4">
                                <select class="form-control select2" name="type" style="width: 100%;">
                                    <option value="card">Carteirinha</option>
                                    <option value="proposta">Proposta</option>
                                    <option value="cpf">CPF</option>
                                    <option value="nome">Titular/Beneficiarios</option>
                                </select>
                            </div>
                            <div class="col-xs-5">
                                <button class="btn btn-primary btn-flat buscar">Buscar</button>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
            <div class="col-xs-06">
                <div class="response"></div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/agente_brasil_sys.css')}}">
@stop

@section('js')
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/blockUI.js') }}"></script>
    <script>
        $(function () {
            var tableBuscas = function(data){
                html = "<div class='col-md-09'>";
                html +='<table class="table table-hover tableHistorico">' +
                        '<caption><b>Situacao</b> '+data.situacao+'<br><b>Historico</b></caption>'+
                        '<thead><tr><th>Carteira</th><th>Nome</th><th>Status</th><th>Tipo</th><th>Telefone</th><th>Data de Inclusão/Exclusão</th></tr>';

                html += "<tbody>";
                    var listaBene = data.beneficiarios.slice(2);
                    listaBene.reverse();
                    for(i=0;i<listaBene.length;i++) {

                        var classe = "";
                        if(listaBene[i].Beneficiarios[3].Situacao=="Excluído"){
                            classe = "dangerAgenteLogError";
                        }

                        html +=
                                "<tr class='"+classe+"'>"+
                                "<td>" + listaBene[i].Beneficiarios[0].NumeroAssociado + "</td>" +
                                "<td>" + listaBene[i].Beneficiarios[1].Nome + "</td>" +
                                "<td>" + listaBene[i].Beneficiarios[3].Situacao + "</td>" +
                                "<td>" + listaBene[i].Beneficiarios[2].Tipo + "</td>" +
                                "<td>" + listaBene[i].Beneficiarios[5].Telefeone + "</td>" +
                                "<td>" + listaBene[i].Beneficiarios[4].DataInclusao + "</td>" +
                                "</tr>";
                    }
                html += "</tbody>" +
                        "</table>"+
                "</div>";
                return html;
            };

            $('.buscar').on('click', function (e) {
                e.preventDefault();
                if($('#value').val() != "") {
                    $.blockUI({message:"Processando..."});
                    var promise = $.ajax({
                        method: "post",
                        url: $('.findUnimed').attr('action'),
                        data: {
                            type: $('.select2').val(), //type
                            value: $('input[name=value]').val()
                        }
                    });
                } else {
                    alert('Preencha o campo value');
                }
                promise.done(function (resp) {
                    $.unblockUI();
                    $('.response').html(tableBuscas(resp));
                    $('.tableHistorico').DataTable();
                });
                promise.fail(function (resp) {
                    $.unblockUI();
                    alert(resp.responseJSON);
                    $('.response').html(resp.responseJSON);
                });
            });
        });
    </script>
@stop
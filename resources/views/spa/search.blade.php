<html>
<head>
    <title>UNIMED | SYS | AGENTEBRASIL</title>
<link rel="stylesheet" href="https://agentebrasil.com.br/start/bootstrap.min.css">
<link rel="stylesheet" href="https://agentebrasil.com.br/start/bootstrap-theme.min.css">
<link rel="stylesheet" href="https://agentebrasil.com.br/start/theme.css">
<link href='https://fonts.googleapis.com/css?family=Oswald&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Lato:700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:400,500,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Nunito:700' rel='stylesheet' type='text/css'>
</head>
<script src="https://agentebrasil.com.br/start/jquery-1.7.1.min.js" ></script>
<style>
.cynthia{
font-family: Oswald;font-size:58px;
}
.center {
    margin: auto;
    width: 400px;
    padding: 10px;
}
</style>
    <script type="text/javascript">
 	$(document).ready(function(){
 		$('#buscar').click(function(){
 			numero_proposta=$('#proposta').val();
 			numero_proposta=$.trim(numero_proposta);
 			$('#proposta').val(numero_proposta);
 			type=$('#type').val();
 			if(numero_proposta != ''){
 				objeto_finder={type: type,str:numero_proposta};
 				$('#resultado').html('<img src="load.gif" />');
				$.post(  
					"xml2.php",  
					objeto_finder,
					function(result){		
							console.log(result);
							if(type=='card'){
								//console.log(result.card_id);
								result.fone=result.fone.replace(' ','');
								$('#resultado').html('Carteira: <b>'+result.card_id+'</b><br>Nome: <b>'+result.nome+'</b><br>Situacao:<b> '+result.situacao+'</b><br>Inclusao: <b>'+result.inclusao+'</b><br>fone:<b> '+result.fone+'</b>');
								load_lead('leads','lead_nome',result.nome,'lead',result.card_id);
							}else{
								$('#resultado').html('');
								c=0;
								proposta=result.proposta;
									$.each(result.results,function(i,field){
										c++;
										if(field.situacao.indexOf('Exclu') != -1){
											situacao='<span style="color:red;">'+field.situacao+'</span>';
										}else{
											situacao='<span style="color:green;">'+field.situacao+'</span>';
										}
										$('#resultado').append('<br><span class="n_proposta" data-proposta="'+proposta+'" style="cursor:pointer;background:green;color:white;padding:2px;">'+proposta+'</span> - '+field.nome+
																' - <b> <span class="n_card" data-card="'+field.card_id+'" style="font-weight:bold;cursor:pointer;padding:1px;background:black;color:white;">'+field.card_id+'</span> '+situacao+' - '+field.inclusao+' - '+field.fone);
									});
								if(c==0){
									$('#resultado').html('<br><b>Not found</b>');
								}
							}
						},'json'
				);
 			}
 		});
 		// CLICK NUMERO PROPOSTA ----------------------------------
 		$(document).on('click','.n_proposta',function(){
 			prop=$(this).attr('data-proposta');
 			$('#proposta').val(prop);
 			$('#type').val('proposta');
 			$('#buscar').trigger('click');
 		});
 		// CLICK CARTAO UNIMED ----------------------------------
 		$(document).on('click','.n_card',function(){
 			prop=$(this).attr('data-card');
 			$('#proposta').val(prop);
 			$('#type').val('card');
 			$('#buscar').trigger('click');
 		});
 		$(document).keypress(function(e) {
 			console.log('enter');
 		    if(e.which == 13 && $('#pass').val() != '') {
 		        $('#buscar').trigger('click');
 		    }
 		});
 	});
 	
 	//###############  BUSCA EM LEAD, BENEFICIARIO, TITULAR ###############################
 	function load_lead(TABLE,COLUMN_NAME,VALUE,PRE,CARD){
			console.log(TABLE+' ----------------------- ');
			c=0;
			$.post(  
					"action.php",  
					{
						'a':'load_one',
						'ID_mysql':COLUMN_NAME,
						'myT':TABLE,
						'id':VALUE
					},
					function(result){		
						$.each(result.clientes,function(i,field){
							if(field.nome != 'fim'){
								$('#resultado').append('<br><br>AB DATABASE <b>'+TABLE.toUpperCase()+'</b>: <B>'+field[PRE+'_nome'].toUpperCase()+' - '+field.card_id+'</B>');
								
									update_lead(TABLE,COLUMN_NAME,VALUE,CARD);
									c++;
								
							}else{
							}
						});
						console.log('c:'+c);
						if(c==0 && TABLE=='leads'){
							load_lead('beneficiarios','bene_nome',VALUE,'bene',CARD);
						}
						if(c==0 && TABLE=='beneficiarios'){
							load_lead('titulares','titu_nome',VALUE,'titu',CARD);
						}
					},'json'
			);
		}

 		// ################### UPDATE CARD_ID EM LEADS, BENEFICIARIOS E TITULARES ###########################
 		function update_lead(TABLE,COLUMN_NAME,VALUE,CARD){
	 		uarr={};
			uarr['card_id']=CARD;
			array_string=uarr;
			for(i in uarr){
				uarr[i]=$.trim(uarr[i]);
			}
			VALUE='"'+VALUE+'"';
			if(CARD != ''){
				$.post(
						"action.php",
						{a:'edit_ninja_like',id:VALUE,ID_mysql:COLUMN_NAME,arr:array_string,myT:TABLE},
						function(result){
							console.log(result);
						}
				);
			}
 		}
 		
 		
 		
    </script>
</head>
<body>
<span>AGENTEBRASIL | UNIMED<BR>CONSULTA POR PROPOSTA E VISUALIZACAO DE NUMERO DEFINITIVO DE CARTEIRA</span><br><br><br>
        <div class="form-inline " style="text-align:center;">
        <label>CONSULTA UNIMED: </label><br>
            <input id="proposta" class="form-control " style="width:400px"/>
            <select id="type"  class="form-control ">
            	<option value="card">Carteirinha</option>
            	<option value="proposta">Proposta</option>
            	<option value="cpf">CPF</option>
            	<option value="nome">Titular/Beneficiarios</option>
            </select>
            <br>
            <br>
            <button id="buscar"  class="btn btn-sm btn-success   ">CONSULTAR</button>
        </div>
        <div id="resultado" style="text-align:center;"></div>
</body>
<html>
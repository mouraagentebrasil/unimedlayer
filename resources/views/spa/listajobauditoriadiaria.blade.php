@extends('adminlte::page')

@section('content_header')
    <h1>Auditoria Diaria Automatica</h1>
@stop

@section('content')
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title info">Listando Resultados</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-header with-border">
            <form class="buscaAuditorias" action="{{ url('/getAuditoriasAutomaticas')}}" data-urlgeneraterel="{{ url('/generateRel') }}" data-urldonwloadrel="{{ url('downloadRel') }}">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Data Auditoria</label>
                            <div class="input-group date col-md-6">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" name="date" class="form-control pull-right" id="date" value="{{date( 'Y-m-d',strtotime('-1 day') )}}">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button type="button" class="buscaAuditoriasBtn btn btn-success btn-flat">Buscar Auditorias</button>
                </div>
            </form>
            <div class="col-xs-06">
                <div class="gerarRelatorio"></div>
                <div class="response1">
                    <table class="table table-hover dataTable">
                        <thead>
                        <tr>
                            <th>Lead ID: </th>
                            <th>Tipo: </th>
                            <th>ID_TIPO: </th>
                            <th>Carteira</th>
                            <th>Status Valor Associado</th>
                            <th>Descricao do Erro</th>
                            <th>Data do Processamento</th>
                        </tr>
                        </thead>
                        <tbody class="response">
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/agente_brasil_sys.css')}}">
    <link rel="stylesheet" href="{{asset('js/Datepicker/datepicker.css')}}">
@stop

@section('js')
    <script src="{{ asset('js/Datepicker/datepicker.js') }}"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/blockUI.js') }}"></script>
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/locale/pt-br.js"></script>--}}
    <script>
        $(function () {
            $('#date').datepicker({
                "useCurrent": true,
                "setDate": new Date(),
                "autoclose": true,
                "language": 'pt',
                "format": "yyyy-mm-dd"
            });

            //inicia a listagem com as ultimas 30 auditorias
            var promiseInitListagem = $.ajax({method:"get",url:$('.buscaAuditorias').attr('action')});
            $.blockUI({"message":"Processando"});
            promiseInitListagem.done(function(resp){
                $.unblockUI();
                $('.response').html(montaTable(resp));
                $('.gerarRelatorio').html(gerarRelatorioCsv(resp));
                $('.dataTable').DataTable({processing : true});
            });

            promiseInitListagem.fail(function(resp){
                $.unblockUI();
                $('.response').html(responseJSON.message);
            });

            $('.buscaAuditoriasBtn').on('click',function(e){
                //var date = $('#datepicker_data_final').val();
                var url = $('.buscaAuditorias').attr('action')+"/"+$('#date').val();

                var promiseListagem = $.ajax({method:"get",url:url});
                promiseListagem.done(function(resp){
                    $('.response').html(montaTable(resp));
                    $('.gerarRelatorio').html(gerarRelatorioCsv(resp));
                    $('.dataTable').DataTable();
                });
                promiseListagem.fail(function(resp){
                    console.log(resp)
                    alert(resp.responseJSON.message);
                    //$('.response').html(responseJSON.message);
                });
            });
            montaTable = function(data){
                html = "";
                for(i=0;i<data.length;i++){
                    html +=
                            '<tr class="dangerAgenteLogError">'+
                            '<td>'+data[i].lead_id+'</td>'+
                            '<td>'+data[i].tipo+'</td>'+
                            '<td>'+data[i].id_tipo+'</td>'+
                            '<td>'+data[i].card_unimed+'</td>'+
                            '<td>'+data[i].status_valor+'</td>'+
                            '<td>'+data[i].desc_erro+'</td>'+
                            '<td>'+data[i].data_processamento+'</td>'+
                            '</td>';
                }
                return html;
            };

            var gerarRelatorioCsv = function(data){
                html = '<button type="button" class="gerarRelatorioBtn btn btn-primary btn-flat" ' +
                        "data-rel='"+JSON.stringify(data)+"' style='float:right; margin-bottom: 8px;'>" +
                        '<i class="fa fa-fw fa-cogs"></i>'+
                        'Gerar Relatorio' +
                        '</button>';
                return html;
            };

            $(document).on('click','.gerarRelatorioBtn',function(){
                var stringifyRel = JSON.stringify($('.gerarRelatorioBtn').data('rel'));
                var promise = $.ajax({method:'post','url':$('.buscaAuditorias').data('urlgeneraterel'),data: {'objRel':stringifyRel}});
                $.blockUI({"message":"Processando Relatorio"});
                promise.done(function(resp){
                    window.open($('.buscaAuditorias').data('urldonwloadrel')+"/"+resp,'_blank');
                    $.unblockUI();
                });
                promise.fail(function (resp) {
                    $.unblockUI();
                    alert('Erro no servidor!');
                });
            })
        });
    </script>
@stop
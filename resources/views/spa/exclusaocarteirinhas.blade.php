@extends('adminlte::page')

@section('content_header')
    <h1>Exclusão de carteiras</h1>
@stop

@section('content')
    <div class="box">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Preencha os campos</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" class="formExclusaoCarteira" action="{{url('/excluiCarteira')}}">
                <div class="box-body">
                    <div class="form-group">
                        <label>Cartão Unimed</label>
                        <input type="text" class="form-control" id="" name="card_unimed" placeholder="Numero Cartão Unimed">
                    </div>
                    <div class="form-group">
                        <label>Data Exclusão</label>
                        <div class="input-group date col-md-3">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" name="data_exclusao" class="form-control pull-right" id="datepicker" value="{{date('d/m/Y')}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Observação</label>
                        <input type="text" class="form-control" id="" name="observacao_exclusao" maxlength="22" placeholder="Frase até 22 caracteres">
                    </div>
                    <div class="form-group">
                        <label>Motivo</label>
                        <select class="form-control" name="motivo_exclusao" id="motivo_exclusao" class="form-control ">
                            <option value="46">Inclusão indevida</option>
                            <option value="93">Inadimplência</option>
                            <option value="10">A pedido do beneficiário</option>
                            <option value="81">Troca Titularidade</option>
                            <option value="5">Falecimento</option>
                            <option value="8">Transferência Carteira</option>
                            <option value="31">Aposentadoria</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Origem da Solicitação</label>
                        <select id="origem_exclusao" name="origem_exclusao" class="form-control ">
                            <option value="sac">SAC</option>
                            <option value="vendas">Vendas</option>
                            <option value="financeiro">Financeiro</option>
                            <option value="supervisor">Supervisor</option>
                            <option value="diretoria">Diretoria</option>
                            <option value="outros">Outros</option>
                        </select>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-danger btn-flat excluir">Excluir</button>
                    <div class="col-xs-06">
                        <div class="response"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/agente_brasil_sys.css')}}">
    <link rel="stylesheet" href="{{asset('js/Datepicker/datepicker.css')}}">
@stop

@section('js')
    <script src="{{ asset('js/Datepicker/datepicker.js') }}"></script>
    <script src="{{ asset('js/blockUI.js') }}"></script>
    <script>
        $(function () {
            $('#datepicker').datepicker({
                "useCurrent": true,
                "setDate": new Date(),
                "autoclose": true,
                "language": 'pt',
                "format": "dd/mm/yyyy"
            });
            validForm = function () {
                if(
                        $('input[name=card_unimed]').val() == "" ||
                        $('input[name=data_exclusao]').val() == "" ||
                        $('input[name=observacao_exclusao]').val() == "" ||
                        $('#motivo_exclusao').val() == "" ||
                        $('#origem_exclusao').val() == ""
                ) {
                    return true;
                }
                return false;
            };

            $('.formExclusaoCarteira').on('submit',function(e){
                e.preventDefault();
                if(!validForm()) {
                    $.blockUI({message:"Processando..."});
                    var promise = $.ajax({
                        url: $('.formExclusaoCarteira').attr('action'),
                        method: 'post',
                        data: {
                            'card_unimed': $('input[name=card_unimed]').val(),
                            'data_exclusao': $('input[name=data_exclusao]').val(),
                            'observacao_exclusao': $('input[name=observacao_exclusao]').val(),
                            'motivo_exclusao': $('#motivo_exclusao').val(),
                            'origem_exclusao': $('#origem_exclusao').val()
                        }
                    });
                    promise.done(function (resp) {
                        $.unblockUI();
                        $('.response').html(montaTable(resp));
                    });

                    promise.fail(function (resp) {
                        $.unblockUI();
                        alert('Erro, verifique se essa Carteira existe');
                        console.log(resp);
                    });
                } else {
                    alert('Preencha os campos');
                }
            });

            montaTable = function(data){
                var classe = "";
                if(data.status == null){
                    classe = "dangerAgenteMaxError";
                } else if(data.status=="error"){
                    classe = "dangerAgenteLogError";
                }
                html = "<div class='col-md-12'>";
                html +='<table class="table table-hover">' +
                            '<thead>' +
                                '<tr>' +
                                    '<th>LEAD CPF: </th>'+
                                    '<th>Tipo Cadastro (AgenteBrasil): </th>'+
                                    '<th>Tipo Cadastro (Unimed): </th>'+
                                    '<th>Carteira</th>' +
                                    '<th>Nome</th>' +
                                    '<th>Status</th>' +
                                    '<th>Retorno do Servidor</th>' +
                                    '<th>Proposta</th>' +
                                    '<th>Data de Exclusão</th>' +
                                '</tr>'+
                            '</thead>'+
                        '<tbody>'+
                            '<tr class="'+classe+'">'+
                                '<td>'+data.lead_cpf+'</td>'+
                                '<td>'+data.tipo_cadastro_agente_brasil+'</td>'+
                                '<td>'+data.tipo_cadastro_unimed+'</td>'+
                                '<td>'+data.carteira+'</td>'+
                                '<td>'+data.nome+'</td>'+
                                '<td>'+data.status+'</td>'+
                                '<td>'+data.message+'</td>'+
                                '<td>'+data.proposta+'</td>'+
                                '<td>'+data.data_inclusao+'</td>'+
                            '</tr>'+
                        '</tbody>' +
                        '</table></div>';
                return html;
            }
        });
    </script>
@stop
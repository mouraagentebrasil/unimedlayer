@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/square/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/css/auth.css') }}">
    @yield('css')
@stop

@section('body_class', 'login-page')

@section('body')
    <div class="login-box">
        <div class="login-logo">
            <a href="#">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg">Faça Login para entrar no SYS-V2</p>
            <form action="{{url('/auth')}}" class="submitLoginForm" method="post">
                <?php $message = Session::get('message'); ?>
                @if (isset($message))
                <div class="alert alert-danger alert-dismissible timeOut">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    {{$message}}
                </div>
                @endif
                <div class="form-group has-feedback">
                    <input type="email" name="login" class="form-control" value="{{ old('login') }}"
                           placeholder="Digite seu email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="pass" class="form-control"
                           placeholder="Digite sua senha">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit"
                                class="btn btn-primary btn-block btn-flat submitLogin">Login</button>
                    </div>
                    <!-- /.col -->
                </div>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div><!-- /.login-box -->
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        $(function () {
            //controla os campos do login
            $('.submitLogin').on('click',function(e){
               e.preventDefault();
                if($('input[name=login]').val()!=="" || $('input[name=pass]').val()!=="" ){
                    $('.submitLoginForm').submit();
                } else {
                    $('input[name=login]').focus();
                    alert('Preencha os campos para fazer login');
                }
            });
            //remove o feedback depois de 3s
            setTimeout(function(){
                $('.timeOut').hide("slow");
            }, 3000);
        });
    </script>
    @yield('js')
@stop
